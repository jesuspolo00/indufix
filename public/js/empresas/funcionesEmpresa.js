$(document).ready(function () {
	var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');

	$(".inactivar_empresa").on(tipoEvento, function () {
		var id = $(this).attr('id');
		inactivarEmpresa(id);
	});

	$(".activar_empresa").on(tipoEvento, function () {
		var id = $(this).attr('id');
		activarEmpresa(id);
	});

	$(".eliminar_empresa").on(tipoEvento, function () {
		var id = $(this).attr('id');
		eliminarEmpresa(id);
	});

	function inactivarEmpresa(id) {
		try {
			$.ajax({
				url: '../vistas/ajax/empresas/inactivarEmpresa.php',
				method: 'POST',
				data: { 'id_empresa': id },
				cache: false,
				success: function (resultado) {
					if (resultado == 'ok') {
						$('#' + id + '').removeAttr('title');
						$('#' + id + '').removeClass('btn-danger inactivar_empresa').addClass('btn-success activar_empresa');
						$('#' + id + ' i').removeClass('fa-times').addClass('fa-check');
						ohSnap("Inactivado correctamente!", { color: "yellow", 'duration': '1000' });
						setTimeout(recargarPagina, 1050);
					} else {
						ohSnap("ha ocurrido un error!", { color: "red", 'duration': '1000' });
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}


	function activarEmpresa(id) {
		try {
			$.ajax({
				url: '../vistas/ajax/empresas/activarEmpresa.php',
				method: 'POST',
				data: { 'id_empresa': id },
				cache: false,
				success: function (resultado) {
					if (resultado == 'ok') {
						$('#' + id + '').removeAttr('title');
						$('#' + id + '').removeClass('btn-success activar_empresa').addClass('btn-danger inactivar_empresa');
						$('#' + id + ' i').removeClass('fa-times').addClass('fa-times');
						ohSnap("Activado correctamente!", { color: "yellow", 'duration': '1000' });
						setTimeout(recargarPagina, 1050);
					} else {
						ohSnap("ha ocurrido un error!", { color: "red", 'duration': '1000' });
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}


	function eliminarEmpresa(id) {
		try {
			$.ajax({
				url: '../vistas/ajax/empresas/eliminarEmpresa.php',
				method: 'POST',
				data: { 'id_empresa': id },
				cache: false,
				success: function (resultado) {
					if (resultado == 'ok') {
						ohSnap("Eliminado correctamente!", { color: "green", 'duration': '1000' });
						$('#empresa' + id).fadeOut();
					} else {
						ohSnap("ha ocurrido un error!", { color: "red", 'duration': '1000' });
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}


	function recargarPagina() {
		window.location.replace('index');
	}

});