$(document).ready(function () {

	var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');

	$(".inactivar_sede").on(tipoEvento, function () {
		var id = $(this).attr('id');
		inactivarSede(id);
	});

	$(".activar_sede").on(tipoEvento, function () {
		var id = $(this).attr('id');
		activarSede(id);
	});

	$(".eliminar_sede").on(tipoEvento, function () {
		var id = $(this).attr('id');
		eliminarSede(id);
	});

	$("#id_empresa").change(function () {
		var id = $(this).val();
		var campo = $(this);
		mostrarNit(id, campo);
	});


	function mostrarNit(id) {
		try {
			$.ajax({
				url: '../vistas/ajax/sedes/mostrarNitEmpresa.php',
				method: 'POST',
				data: { 'id_empresa': id },
				cache: false,
				success: function (resultado) {
					console.log(resultado);
					if (resultado != '') {
						$("#nitE").val(resultado.replace(/['"]+/g, ''));
					} else {
						ohSnap("ha ocurrido un error!", { color: "red", 'duration': '1000' });
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}


	function inactivarSede(id) {
		try {
			$.ajax({
				url: '../vistas/ajax/sedes/inactivarSede.php',
				method: 'POST',
				data: { 'id_sucursal': id },
				cache: false,
				success: function (resultado) {
					console.log(resultado);
					if (resultado == 'ok') {
						$('#' + id + '').removeAttr('title');
						$('#' + id + '').removeClass('btn btn-danger btn-sm inactivar_sede').addClass('btn btn-success btn-sm activar_sede');
						$('#' + id + ' i').removeClass('fa-times').addClass('fa-check');
						ohSnap("Inactivado correctamente!", { color: "yellow", 'duration': '1000' });
						setTimeout(recargarPagina, 1050);
					} else {
						ohSnap("ha ocurrido un error!", { color: "red", 'duration': '1000' });
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}


	function activarSede(id) {
		try {
			$.ajax({
				url: '../vistas/ajax/sedes/activarSede.php',
				method: 'POST',
				data: { 'id_sucursal': id },
				cache: false,
				success: function (resultado) {
					if (resultado == 'ok') {
						$('#' + id + '').removeAttr('title');
						$('#' + id + '').removeClass('btn btn-success btn-sm activar_sede').addClass('btn btn-danger btn-sm inactivar_sede');
						$('#' + id + ' i').removeClass('fa-times').addClass('fa-times');
						ohSnap("Activado correctamente!", { color: "yellow", 'duration': '1000' });
						setTimeout(recargarPagina, 1050);
					} else {
						ohSnap("ha ocurrido un error!", { color: "red", 'duration': '1000' });
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}

	function eliminarSede(id) {
		try {
			$.ajax({
				url: '../vistas/ajax/sedes/eliminarSede.php',
				method: 'POST',
				data: { 'id_sucursal': id },
				cache: false,
				success: function (resultado) {
					if (resultado == 'ok') {
						ohSnap("Eliminado correctamente!", { color: "green", 'duration': '1000' });
						$('#empresa' + id).fadeOut();
					} else {
						ohSnap("ha ocurrido un error!", { color: "red", 'duration': '1000' });
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}


	function recargarPagina() {
		window.location.replace('index');
	}

});