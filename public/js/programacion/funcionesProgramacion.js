$(document).ready(function(){

	var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');

	$("#empresa").change(function(){
		var id = $(this).val();
		var campo = $(this);
		mostrarCiudadEmpresa(id,campo);
		mostrarSucursalesEmpresa(id,campo);
	});


	$(".check").on(tipoEvento, function(){
		var id = $(this).attr('id');
		if( $(this).is(':checked') ){
			$("#num_" + id).attr('readonly', false);
		} else {
			$("#num_" + id).attr('readonly', true);
		}
	});


	$("#hora_fin").change(function(){
		var hora_inicio = $("#hora_inicio").val();
		var valor = $(this).val();
		var dt = new Date();
		var time = dt.getHours() + ":" + dt.getMinutes();
		if(valor <= hora_inicio){
			$(".tooltip").hide();
			$(".tooltip").show();
			$("#hora_fin").focus();
			$("#hora_fin").attr('data-toggle', 'tooltip');
			$("#hora_fin").addClass('border border-danger');
			$("#hora_fin").tooltip({title: "La hora debe ser mayor a la inicial", trigger: "focus", placement: "right"}); 
			$("#hora_fin").tooltip('show');
		}else{
			$("#hora_fin").removeClass('border border-danger').addClass('border border-success');
			$(".tooltip").hide();
		}
	});


	function mostrarCiudadEmpresa(id){
		try {
			$.ajax({
				url: '../vistas/ajax/empresas/mostrarCiudadEmpresa.php',
				method: 'POST',
				data: {'id_empresa': id},
				cache: false,
				success: function (resultado) {
					if (resultado != '') {
						$("#ciudad").val(resultado.replace(/['"]+/g, ''));
					} else {
						ohSnap("ha ocurrido un error!", {color: "red", 'duration': '1000'});
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}


	function mostrarSucursalesEmpresa(id){
		$('#sucursales').attr('disabled', false);
		$('#sucursales').html('');
		try {
			$.ajax({
				url: '../vistas/ajax/empresas/mostrarSucursalesEmpresa.php',
				method: 'POST',
				data: {'id_empresa': id},
				cache: false,
				dataType:'JSON',
				success: function (resultado) {
					if (resultado != '') {
						resultado.forEach(function(element) {
							$('#sucursales').append('<option value="'+element['id_sucursal']+'">'+element['nombre']+' ('+element['nit']+')</option>')
						});
					} else {
						$('#sucursales').attr('disabled', true);
						ohSnap("No hay datos para mostrar!", {color: "red", 'duration': '1000'});
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}
});