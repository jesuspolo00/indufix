$(document).ready(function(){

	var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');

	$(".inactivar_residuo").on(tipoEvento, function(){
		var id = $(this).attr('id');
		inactivarResiduo(id);
	});


	$(".activar_residuo").on(tipoEvento, function(){
		var id = $(this).attr('id');
		activarResiduo(id);
	});


	$(".eliminar_residuo").on(tipoEvento, function(){
		var id = $(this).attr('id');
		eliminarResiduo(id);
	});


	function inactivarResiduo(id){
		try {
			$.ajax({
				url: '../vistas/ajax/residuos/inactivarResiduo.php',
				method: 'POST',
				data: {'id_residuo': id},
				cache: false,
				success: function (resultado) {
					if (resultado == 'ok') {
						$('#'+id+'').removeAttr('title');
						$('#'+id+'').removeClass('btn-danger inactivar_residuo').addClass('btn-success activar_residuo');
						$('#'+id+' i').removeClass('fa-times').addClass('fa-check');
						ohSnap("Inactivado correctamente!", {color: "yellow", 'duration': '1000'});
						setTimeout(recargarPaginaTipo,2000);
					} else {
						ohSnap("ha ocurrido un error!", {color: "red", 'duration': '1000'});
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}


	function activarResiduo(id){
		try {
			$.ajax({
				url: '../vistas/ajax/residuos/activarResiduo.php',
				method: 'POST',
				data: {'id_residuo': id},
				cache: false,
				success: function (resultado) {
					if (resultado == 'ok') {
						$('#'+id+'').removeAttr('title');
						$('#'+id+'').removeClass('btn-success activar_residuo').addClass('btn-danger inactivar_residuo');
						$('#'+id+' i').removeClass('fa-check').addClass('fa-times');
						ohSnap("Activado correctamente!", {color: "yellow", 'duration': '1000'});
						setTimeout(recargarPaginaTipo,2000);
					} else {
						ohSnap("ha ocurrido un error!", {color: "red", 'duration': '1000'});
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}

	function eliminarResiduo(id){
		try {
			$.ajax({
				url: '../vistas/ajax/residuos/eliminarResiduo.php',
				method: 'POST',
				data: {'id_residuo': id},
				cache: false,
				success: function (resultado) {
					if (resultado == 'ok') {
						ohSnap("Eliminado correctamente!", {color: "green", 'duration': '1000'});
						$('#tipo_residuo'+id).fadeOut();
					} else {
						ohSnap("ha ocurrido un error!", {color: "red", 'duration': '1000'});
					}
				}
			});
		} catch (evt) {
			alert(evt.message);
		}
	}

	function recargarPaginaTipo(){
		window.location.replace("index");
	}

});