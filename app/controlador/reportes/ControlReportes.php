<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'reportes' . DS . 'ModeloReportes.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreos.php';
require_once MODELO_PATH . 'usuario' . DS . 'ModeloUsuario.php';
require_once MODELO_PATH . 'vehiculo' . DS . 'ModeloVehiculo.php';

class ControlReportes
{

	private static $instancia;

	public static function singleton_reportes()
	{
		if (!isset(self::$instancia)) {
			$miclase = __CLASS__;
			self::$instancia = new $miclase;
		}
		return self::$instancia;
	}


	public function MostrarReportesDanoControl($super_empresa)
	{
		$mostrar = ModeloReportes::MostrarReportesDanoModelo($super_empresa);
		return $mostrar;
	}


	public function MostrarReportesMantControl($super_empresa)
	{
		$mostrar = ModeloReportes::MostrarReportesMantModelo($super_empresa);
		return $mostrar;
	}


	public function MostrarReportesOtroControl($super_empresa)
	{
		$mostrar = ModeloReportes::MostrarReportesOtroModelo($super_empresa);
		return $mostrar;
	}

	public function MostrarReportesIdControl($id,$super_empresa)
	{
		$mostrar = ModeloReportes::MostrarReportesIdModel($id,$super_empresa);
		return $mostrar;
	}


	public function mostrarTipoEstadoIdControl($id)
	{
		$mostrar = ModeloReportes::mostrarTipoEstadoIdModel($id);
		return $mostrar;
	}


	public function reportarSolucionControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_resp']) &&
			!empty($_POST['id_resp']) &&
			isset($_POST['id_reporte']) &&
			!empty($_POST['id_reporte']) &&
			isset($_POST['id_inventario']) &&
			!empty($_POST['id_inventario'])
		) {

			$observacion = $_POST['observacion_sol'];

			$fecha_fin_reporte = date('Y-m-d H:i:s');

			$datos = array(
				'id_resp' => $_POST['id_resp'],
				'id_reporte' => $_POST['id_reporte'],
				'fecha_fin_reporte' => $fecha_fin_reporte,
				'observacion' => $observacion
			);

			$solucion = ModeloReportes::reportarSolucionModelo($datos);

			if ($solucion['solucion'] == TRUE) {

				$datos_actualizar = array(
					'id_inventario' => $_POST['id_inventario'],
					'tipo_mant' => 4
				);

				$actualizar_estado = ModeloVehiculo::actualizarEstadoVehiculoModelo($datos_actualizar);


				$datos_vehiculo = ModeloVehiculo::mostrarDatosVehiculosIdModel($_POST['id_inventario']);
				$nombre_vehiculo = $datos_vehiculo['descripcion'];
				$placa = $datos_vehiculo['placa'];
				$modelo = $datos_vehiculo['modelo'];
				$marca = $datos_vehiculo['marca'];
				$responsable = $datos_vehiculo['id_user'];

				$datos_usuario = ModeloUsuario::mostrarDatosUsuariosIdModel($responsable);
				$nombre_usuario = $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'];
				$correo = $datos_usuario['correo'];

				if ($observacion != "") {
					$html = '<h3 style="font-weight:bold;">Observacion:</h3>
				<p style="font-size:15px;">' . $observacion . '</p>
				';
				} else {
					$html = '';
				}

				$mensaje = '
			<p style="font-size:15px;"> 
			Se le informa que el vehiculo <span style="font-weight:bold; text-transform:uppercase;">' . $nombre_vehiculo . '</span> registrado con placa <span style="font-weight:bold; text-transform:uppercase;">' . $placa . '</span> ha sido reparado y se encuentra listo para su uso.
			</p>

			' . $html . '
			';

				$datos_correo = array(
					'asunto' => 'Reporte de vehiculo',
					'correo' => $correo,
					'user' => $nombre_usuario,
					'mensaje' => $mensaje,
					'id_apartado' => 0
				);

				//$enviar_correo = Correo::enviarCorreoModel($datos_correo);


				echo '
			<script>
			ohSnap("Solucionado correctamente!", {color: "green", "duration": "1000"});
			setTimeout(recargarPagina,1050);

			function recargarPagina(){
				window.location.replace("index");
			}
			</script>
			';
			} else {
				echo '
			<script>
			ohSnap("Ha ocurrido un error!", {color: "red"});
			</script>
			';
			}
		} else {
		}
	}
}
