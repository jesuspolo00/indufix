<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'hoja_vida' . DS . 'ModeloHojaVida.php';
require_once MODELO_PATH . 'vehiculo' . DS . 'ModeloVehiculo.php';

class ControlHojaVida
{

	private static $instancia;

	public static function singleton_hoja_vida()
	{
		if (!isset(self::$instancia)) {
			$miclase = __CLASS__;
			self::$instancia = new $miclase;
		}
		return self::$instancia;
	}


	public function mostrarComponenteVehiculoControl($id)
	{
		$mostrar = ModeloHojaVida::mostrarComponenteVehiculoModel($id);
		return $mostrar;
	}

	public function mostrarHojaVidaVehiculoControl($id)
	{
		$mostrar = ModeloHojaVida::mostrarHojaVidaVehiculoModel($id);
		return $mostrar;
	}


	public function actualizarHojaVidaControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_vehiculo']) &&
			!empty($_POST['id_vehiculo']) &&
			isset($_POST['id_log']) &&
			!empty($_POST['id_log']) &&
			isset($_POST['descripcion']) &&
			!empty($_POST['descripcion']) &&
			isset($_POST['placa']) &&
			!empty($_POST['placa']) &&
			isset($_POST['marca']) &&
			!empty($_POST['marca']) &&
			isset($_POST['modelo']) &&
			!empty($_POST['modelo'])
		) {
			$fechareg = date('Y-m-d H:i:s');

			$mantenimiento = ($_POST['mantenimiento'] != "") ? $_POST['mantenimiento'] : 0;
			$adquirido = ($_POST['adquirido'] != "") ? $_POST['adquirido'] : '0000-00-00';
			$garantia = ($_POST['garantia'] != "") ? $_POST['garantia'] : '0000-00-00';
			$cont_garantia = ($_POST['cont_garantia'] != "") ? $_POST['cont_garantia'] : '';

			$editar_vehiculo = array(
				'id_vehiculo' => $_POST['id_vehiculo'],
				'id_log' => $_POST['id_log'],
				'descripcion' => $_POST['descripcion'],
				'placa' => $_POST['placa'],
				'marca' => $_POST['marca'],
				'modelo' => $_POST['modelo']
			);

			$guardar = ModeloVehiculo::editarVehiculoModelo($editar_vehiculo);

			if ($guardar == TRUE) {

				$datos_hoja = array(
					'id_vehiculo' => $_POST['id_vehiculo'],
					'mantenimiento' => $mantenimiento,
					'fecha_adquisicion' => $adquirido,
					'fecha_vence' => $garantia,
					'cont_garantia' => $cont_garantia,
					'fecha_update' => $fechareg,
					'id_log' => $_POST['id_log']
				);


				$editar_hoja = ModeloHojaVida::actualizarHojaVidaModel($datos_hoja);


				if ($editar_hoja == TRUE) {
					echo '
				<script>
				ohSnap("Registrado correctamente!", {color: "green", "duration": "1000"});
				setTimeout(recargarPagina,1050);

				function recargarPagina(){
					window.location.replace("index?vehiculo=' . base64_encode($_POST['id_vehiculo']) . '");
				}
				</script>
				';
				} else {
					echo '
				<script>
				ohSnap("Ha ocurrido un error!", {color: "red"});
				</script>
				';
				}
			} else {
				echo '
			<script>
			ohSnap("Ha ocurrido un error!", {color: "red"});
			</script>
			';
			}
		} else {
			echo '
		<script>
		ohSnap("Ha ocurrido un error!", {color: "red"});
		</script>
		';
		}
	}



	public function registrarComponenteHojaVidaControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['descripcion_componente']) &&
			!empty($_POST['descripcion_componente']) &&
			isset($_POST['id_vehiculo']) &&
			!empty($_POST['id_vehiculo']) &&
			isset($_POST['fabricante']) &&
			!empty($_POST['fabricante']) &&
			isset($_POST['licencia']) &&
			!empty($_POST['licencia']) &&
			isset($_POST['fecha']) &&
			!empty($_POST['fecha']) &&
			isset($_POST['id_log']) &&
			!empty($_POST['id_log'])
		) {

			$fechareg = date('Y-m-d H:i:s');

			$datos = array(
				'id_vehiculo' => $_POST['id_vehiculo'],
				'descripcion' => $_POST['descripcion_componente'],
				'fabricante' => $_POST['fabricante'],
				'licencia' => $_POST['licencia'],
				'fecha' => $_POST['fecha'],
				'fechareg' => $fechareg,
				'id_log' => $_POST['id_log']
			);


			$guardar = ModeloHojaVida::registrarComponenteHojaVidaModel($datos);

			if ($guardar = TRUE) {
				echo '
		<script>
		ohSnap("Registrado correctamente!", {color: "green", "duration": "1000"});
		setTimeout(recargarPagina,1050);

		function recargarPagina(){
			window.location.replace("index?vehiculo=' . base64_encode($_POST['id_vehiculo']) . '");
		}
		</script>
		';
			} else {
				echo '
		<script>
		ohSnap("Ha ocurrido un error!", {color: "red"});
		</script>
		';
			}
		} else {
			echo '
	<script>
	ohSnap("Ha ocurrido un error!", {color: "red"});
	</script>
	';
		}
	}
}
