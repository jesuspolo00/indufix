<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'residuos' . DS . 'ModeloResiduos.php';

class ControlResiduos
{

	private static $instancia;

	public static function singleton_residuo()
	{
		if (!isset(self::$instancia)) {
			$miclase = __CLASS__;
			self::$instancia = new $miclase;
		}
		return self::$instancia;
	}


	public function mostrarResiduosControl($super_empresa)
	{
		$mostrar = ModeloResiduos::mostrarResiduosModel($super_empresa);
		return $mostrar;
	}

	public function mostrarResiduosIdControl($id)
	{
		$mostrar = ModeloResiduos::mostrarResiduosIdModel($id);
		return $mostrar;
	}

	public function mostrarResiduosTipoIdControl($id)
	{
		$mostrar = ModeloResiduos::mostrarResiduosTipoIdModel($id);
		return $mostrar;
	}

	public function guardarResiduoControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['super_empresa']) &&
			!empty($_POST['super_empresa']) &&
			isset($_POST['id_log']) &&
			!empty($_POST['id_log']) &&
			isset($_POST['descripcion']) &&
			!empty($_POST['descripcion']) &&
			isset($_POST['id_tipo']) &&
			!empty($_POST['id_tipo']) &&
			isset($_POST['valor']) &&
			!empty($_POST['valor'])
		) {

			$fechareg = date('Y-m-d H:i:s');

			$datos = array(
				'nombre' => $_POST['descripcion'],
				'id_tipo' => $_POST['id_tipo'],
				'codigo' => $_POST['codigo'],
				'user_log' => $_POST['id_log'],
				'super_empresa' => $_POST['super_empresa'],
				'valor' => $_POST['valor'],
				'fechareg' => $fechareg
			);

			$guardar = ModeloResiduos::guardarResiduoModel($datos);

			if ($guardar == TRUE) {
				echo '
			<script>
			ohSnap("Registrado correctamente!", {color: "green", "duration": "1000"});
			setTimeout(recargarPagina,1050);

			function recargarPagina(){
				window.location.replace("index");
			}
			</script>
			';
			} else {
				echo '
			<script>
			ohSnap("Ha ocurrido un error!", {color: "red"});
			</script>
			';
			}
		} else {
		}
	}


	public function editarResiduoControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_residuo']) &&
			!empty($_POST['id_residuo']) &&
			isset($_POST['nom_edit']) &&
			!empty($_POST['nom_edit']) &&
			isset($_POST['id_tipo_edit']) &&
			!empty($_POST['id_tipo_edit'])
		) {


			$datos = array(
				'id_residuo' => $_POST['id_residuo'],
				'nombre' => $_POST['nom_edit'],
				'id_tipo' => $_POST['id_tipo_edit'],
				'codigo' => $_POST['cod_edit']
			);

			$editar = ModeloResiduos::editarResiduoModel($datos);

			if ($editar = TRUE) {
				echo '
		<script>
		ohSnap("Modificado correctamente!", {color: "green", "duration": "1000"});
		setTimeout(recargarPagina,1050);

		function recargarPagina(){
			window.location.replace("index");
		}
		</script>
		';
			} else {
				echo '
		<script>
		ohSnap("Ha ocurrido un error!", {color: "red"});
		</script>
		';
			}
		} else {
		}
	}




	public function inactivarResiduoControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_residuo']) &&
			!empty($_POST['id_residuo'])
		) {


			$id_residuo = filter_input(INPUT_POST, 'id_residuo', FILTER_SANITIZE_NUMBER_INT);
			$result = ModeloResiduos::inactivarResiduoModelo($id_residuo);

			if ($result == TRUE) {
				$r = "ok";
			} else {
				$r = "No";
			}
			return $r;
		}
	}



	public function activarResiduoControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_residuo']) &&
			!empty($_POST['id_residuo'])
		) {


			$id_residuo = filter_input(INPUT_POST, 'id_residuo', FILTER_SANITIZE_NUMBER_INT);
			$result = ModeloResiduos::activarResiduoModelo($id_residuo);

			if ($result == TRUE) {
				$r = "ok";
			} else {
				$r = "No";
			}
			return $r;
		}
	}



	public function eliminarResiduoControl()
	{
		if (
			$_SERVER['REQUEST_METHOD'] == 'POST' &&
			isset($_POST['id_residuo']) &&
			!empty($_POST['id_residuo'])
		) {


			$id_residuo = filter_input(INPUT_POST, 'id_residuo', FILTER_SANITIZE_NUMBER_INT);
			$result = ModeloResiduos::eliminarResiduoModelo($id_residuo);

			if ($result == TRUE) {
				$r = "ok";
			} else {
				$r = "No";
			}
			return $r;
		}
	}
}
