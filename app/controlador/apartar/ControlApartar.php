<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'apartar' . DS . 'ModeloApartar.php';
require_once MODELO_PATH . 'residuos' . DS . 'ModeloResiduos.php';
require_once MODELO_PATH . 'correo' . DS . 'ModeloCorreos.php';
require_once MODELO_PATH . 'empresas' . DS . 'ModeloEmpresa.php';
require_once MODELO_PATH . 'empresas' . DS . 'ModeloSede.php';
require_once MODELO_PATH . 'usuario' . DS . 'ModeloUsuario.php';
require_once MODELO_PATH . 'vehiculo' . DS . 'ModeloVehiculo.php';

class ControlApartar
{

  private static $instancia;

  public static function singleton_apartar()
  {
    if (!isset(self::$instancia)) {
      $miclase = __CLASS__;
      self::$instancia = new $miclase;
    }
    return self::$instancia;
  }


  public function mostrarReservasEmpresaIdControl($id)
  {
    $mostrar = ModeloApartar::mostrarReservasEmpresaIdModel($id);
    return $mostrar;
  }


  public function mostrarRecepcionEmpresaIdControl($id)
  {
    $mostrar = ModeloApartar::mostrarRecepcionEmpresaIdModel($id);
    return $mostrar;
  }

  public function confirmadoReservaControl($id)
  {
    $mostrar = ModeloApartar::confirmadoReservaModel($id);
    return $mostrar;
  }

  public function guardarApartadoControl()
  {
    if (
      $_SERVER['REQUEST_METHOD'] == 'POST' &&
      isset($_POST['id_log']) &&
      !empty($_POST['id_log']) &&
      isset($_POST['id_vehiculo']) &&
      !empty($_POST['id_vehiculo']) &&
      isset($_POST['conductor']) &&
      !empty($_POST['conductor']) &&
      isset($_POST['empresa']) &&
      !empty($_POST['empresa']) &&
      isset($_POST['hora_inicio']) &&
      !empty($_POST['hora_inicio']) &&
      isset($_POST['hora_fin']) &&
      !empty($_POST['hora_fin']) &&
      isset($_POST['fecha_apartar']) &&
      !empty($_POST['fecha_apartar']) &&
      $_FILES['hoja_conductor']['name']
    ) {

      $url = base64_encode($_POST['id_vehiculo']);
      $fechareg = date('Y-m-d H:i:s');

      if ($_POST['coopiloto'] != '') {
        $coopiloto = $_POST['coopiloto'];
      } else {
        $coopiloto = 0;
      }


      if ($_POST['sucursal'] != '') {
        $sucursal = $_POST['sucursal'];
      } else {
        $sucursal = 0;
      }


      /*-----------------------------------Validar Horas---------------------------------------------*/

      $fecha_actual = date('Y-m-d');
      $hora_actual = date('H:i');
      $fecha_apartar = $_POST['fecha_apartar'];
      $hora_inicio = $_POST['hora_inicio'];
      $hora_fin = $_POST['hora_fin'];

      $datos_consulta = array(
        'fecha' => $_POST['fecha_apartar'],
        'id_vehiculo' => $_POST['id_vehiculo']
      );

      $registro = ModeloApartar::mostrarApartadosFechaModel($datos_consulta);


      if ($registro['id'] > 0) {
        $hora_inicio_registrada = date('H:i', strtotime($registro['hora_inicio']));
        $hora_fin_registrada = date('H:i', strtotime($registro['hora_fin']));
      } else {
        $hora_inicio_registrada = date('00:00');
        $hora_fin_registrada = date('00:00');
      }


      if ($hora_inicio >= $hora_inicio_registrada && $hora_fin <= $hora_fin_registrada) {
        echo '
          <script>
          ohSnap("Horas ya apartadas!", {color: "red", "duration": "1000"});
          setTimeout(recargarPagina,1050);

          function recargarPagina(){
            window.location.replace("index?vehiculo=' . $url . '");
          }
          </script>
          ';
      } else if ($fecha_apartar < $fecha_actual) {
        echo '
          <script>
          ohSnap("Fecha incorrecta!", {color: "red", "duration": "1000"});
          setTimeout(recargarPagina,1050);

          function recargarPagina(){
            window.location.replace("index?vehiculo=' . $url . '");
          }
          </script>
          ';
      } else if ($hora_inicio <= $hora_inicio_registrada && $hora_fin >= $hora_fin_registrada) {
        echo '
          <script>
          ohSnap("Horas ya apartadas!", {color: "red", "duration": "1000"});
          setTimeout(recargarPagina,1050);

          function recargarPagina(){
            window.location.replace("index?vehiculo=' . $url . '");
          }
          </script>
          ';
      } else if ($hora_inicio < $hora_fin_registrada) {

        echo '
          <script>
          ohSnap("Horas a destiempo!", {color: "red", "duration": "1000"});
          setTimeout(recargarPagina,1050);

          function recargarPagina(){
            window.location.replace("index?vehiculo=' . $url . '");
          }
          </script>
          ';
        /*------------------------------------------------------------------------------*/
      } else {

        $datos = array(
          'id_vehiculo' => $_POST['id_vehiculo'],
          'id_user' => $_POST['id_log'],
          'id_conductor' => $_POST['conductor'],
          'id_empresa' => $_POST['empresa'],
          'id_sucursal' => $sucursal,
          'hora_inicio' => $_POST['hora_inicio'],
          'hora_fin' => $_POST['hora_fin'],
          'fecha_apartado' => $_POST['fecha_apartar'],
          'observacion' => $_POST['observacion'],
          'fechareg' => $fechareg,
          'id_coopiloto' => $coopiloto
        );

        $guardar = ModeloApartar::guardarApartadoModel($datos);

        if ($guardar['guardar'] == TRUE) {

          $id_reserva = $guardar['id'];

          $array_id = array();
          $array_id = $_POST['id_residuo'];

          $array_cantidad = array();
          $array_cantidad = $_POST['cantidad'];

          $it = new MultipleIterator();
          $it->attachIterator(new ArrayIterator($array_id));
          $it->attachIterator(new ArrayIterator($array_cantidad));

          foreach ($it as $a) {

            $mostrar_tipo_residuo = ModeloResiduos::mostrarResiduosIdModel($a[0]);
            $id_tipo_residuo = $mostrar_tipo_residuo['id_tipo_residuo'];

            $datos_apartado_residuo = array(
              'id_apartado' => $id_reserva,
              'id_residuo' => $a[0],
              'id_tipo_residuo' => $id_tipo_residuo,
              'id_log' => $_POST['id_log'],
              'cantidad' => $a[1],
              'fechareg' => $fechareg
            );

            $guardar_residuos = ModeloApartar::guardarResiduosApartarModel($datos_apartado_residuo);
            $guardar_residuos_log = ModeloApartar::apartadoResiduosLogModel($datos_apartado_residuo);
          }

          if ($guardar_residuos == TRUE) {


            /*---------------------------Guardar Documentos----------------------------*/
            if ($_FILES['hoja_conductor']['name'] != '') {
              $archivo = $_FILES['hoja_conductor']['name'];
              $tipo = 1;
              $variable = "hoja_conductor";

              $datos_documento = array(
                'archivo' => $archivo,
                'usuario' => $_POST['conductor'],
                'id_user' => $_POST['id_log'],
                'id_reserva' => $id_reserva,
                'fechareg' => $fechareg,
                'tipo' => $tipo,
                'variable' => $variable,
              );

              $guardar_doc = guardarDocumentoControl($datos_documento);
            }

            if (isset($_FILES['hoja_coopiloto']['name']) && $_FILES['hoja_coopiloto']['name'] != '' && $_POST['coopiloto'] != '') {
              $archivo = $_FILES['hoja_coopiloto']['name'];
              $tipo = 1;
              $variable = "hoja_coopiloto";

              $datos_documento = array(
                'archivo' => $archivo,
                'usuario' => $coopiloto,
                'id_user' => $_POST['id_log'],
                'id_reserva' => $id_reserva,
                'fechareg' => $fechareg,
                'tipo' => $tipo,
                'variable' => $variable,
              );

              $guardar_doc = guardarDocumentoControl($datos_documento);
            }

            if ($_FILES['otros']['name'] != '') {
              $archivo = $_FILES['otros']['name'];
              $tipo = 2;
              $variable = "otros";

              $datos_documento = array(
                'archivo' => $archivo,
                'usuario' => $_POST['id_log'],
                'id_user' => $_POST['id_log'],
                'id_reserva' => $id_reserva,
                'fechareg' => $fechareg,
                'tipo' => $tipo,
                'variable' => $variable,
              );

              $guardar_doc = guardarDocumentoControl($datos_documento);
            }
            /*------------------------------------------------------------------------------------------------*/


            if ($guardar_doc == TRUE) {

              if ($_POST['sucursal'] != '') {
                $datos_empresa = ModeloSede::mostrarSedeIdModel($_POST['sucursal']);
              } else {
                $datos_empresa = ModeloEmpresa::mostrarEmpresaIdModel($_POST['empresa']);
              }

              $nombre_empresa = $datos_empresa['nombre'];
              $correo_empresa = $datos_empresa['email'];


              $datos_conductor = ModeloUsuario::mostrarDatosUsuariosIdModel($_POST['conductor']);
              $nombre_conductor = $datos_conductor['nombre'] . ' ' . $datos_conductor['apellido'];


              $datos_usuario = ModeloUsuario::mostrarDatosUsuariosIdModel($_POST['id_log']);
              $nombre_usuario = $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'];

              $datos_vehiculo = ModeloVehiculo::mostrarDatosVehiculosIdModel($_POST['id_vehiculo']);
              $placa = $datos_vehiculo['placa'];

              if ($_POST['coopiloto'] != '') {
                $datos_coopiloto = ModeloUsuario::mostrarDatosUsuariosIdModel($_POST['coopiloto']);
                $nombre_coopiloto = $datos_coopiloto['nombre'] . ' ' . $datos_coopiloto['apellido'];
                $html = '<li style="font-weight:bold;">Coopiloto: <span style="font-weight:normal;">' . $nombre_coopiloto . '</span></li>';
              } else {
                $html = '';
              }


              $mensaje = '
      <p style="font-size:17px;"> 
      <span style="font-weight:bold;">ECO GREEN RECYCLING S.A.S</span> 
      ha realizado una reserva a nombre de la empresa <span style="font-weight:bold;">' . $nombre_empresa . '</span>
      </p>

      <p>
      <ul  style="font-size:15px;">
      <li style="font-weight:bold;">Conductor: <span style="font-weight:normal;">' . $nombre_conductor . '</span></li>
      ' . $html . '
      <li style="font-weight:bold;">Placa del vehiculo: <span style="font-weight:normal;">' . $placa . '</span></li>
      <li style="font-weight:bold;">Fecha de reserva: <span style="font-weight:normal;">' . $_POST['fecha_apartar'] . '</span></li>
      <li style="font-weight:bold;">Hora Inicio: <span style="font-weight:normal;">' . $hora_inicio . '</span></li>
      <li style="font-weight:bold;">Hora Fin: <span style="font-weight:normal;">' . $hora_fin . '</span></li>
      <li style="font-weight:bold;">Responsable: <span style="font-weight:normal;">' . $nombre_usuario . '</span></li>
      </ul>
      </p>

      ';

              $datos_correo = array(
                'asunto' => 'Apartado de vehiculo',
                'correo' => $correo_empresa,
                'user' => $nombre_empresa,
                'mensaje' => $mensaje,
                'id_apartado' => $id_reserva
              );

              /* $enviar_correo = Correo::enviarCorreoModel($datos_correo);

              if ($enviar_correo == TRUE) { */
              echo '
                  <script>
                  ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                  setTimeout(recargarPagina,1050);

                  function recargarPagina(){
                    window.location.replace("index?vehiculo=' . $url . '");
                  }
                  </script>
                  ';
              /* } else {
                echo '
                  <script>
                  ohSnap("Correo no enviado", {color: "red", "duration": "1000"});
                  </script>
                  ';
              } */
            }
          } else {
            echo '
   <script>
   ohSnap("Residuos no guardados", {color: "red", "duration": "1000"});
   </script>
   ';
          }
        } else {
          echo '
  <script>
  ohSnap("Reserva no guardada", {color: "red", "duration": "1000"});
  </script>
  ';
        }
      }
    } else {
      echo '
 <script>
 ohSnap("Ha ocurrido un error!", {color: "red", "duration": "1000"});
 </script>
 ';
    }
  }





  public function confirmarReservaControl()
  {
    if (
      $_SERVER['REQUEST_METHOD'] == 'POST' &&
      isset($_POST['id_reserva']) &&
      !empty($_POST['id_reserva']) &&
      isset($_POST['id_log']) &&
      !empty($_POST['id_log']) &&
      isset($_POST['empresa']) &&
      !empty($_POST['empresa']) &&
      isset($_POST['hora_fin']) &&
      !empty($_POST['hora_fin'])
    ) {


      if ($_POST['sucursal'] != '') {
        $sucursal = $_POST['sucursal'];
      } else {
        $sucursal = 0;
      }

      $fechareg = date('Y-m-d H:i:s');

      $datos = array(
        'id_reserva' => $_POST['id_reserva'],
        'id_user' => $_POST['id_log'],
        'id_empresa' => $_POST['empresa'],
        'id_sucursal' => $sucursal,
        'hora_fin' => $_POST['hora_fin'],
        'observacion' => $_POST['observacion']
      );

      $guardar = ModeloApartar::confirmarReservaModel($datos);


      if ($guardar == TRUE) {

        $id_reserva = $_POST['id_reserva'];

        $array_id = array();
        $array_id = $_POST['id_residuo'];

        $array_cantidad = array();
        $array_cantidad = $_POST['cantidad'];

        $it = new MultipleIterator();
        $it->attachIterator(new ArrayIterator($array_id));
        $it->attachIterator(new ArrayIterator($array_cantidad));

        $eliminar_residuos = ModeloApartar::eliminarResiduosApartadoModel($_POST['id_reserva']);
        if ($eliminar_residuos == TRUE) {

          foreach ($it as $a) {

            if ($a[1] != 0) {

              $mostrar_tipo_residuo = ModeloResiduos::mostrarResiduosIdModel($a[0]);
              $id_tipo_residuo = $mostrar_tipo_residuo['id_tipo_residuo'];

              $datos_apartado_residuo = array(
                'id_apartado' => $id_reserva,
                'id_residuo' => $a[0],
                'id_tipo_residuo' => $id_tipo_residuo,
                'cantidad' => $a[1],
                'id_log' => $_POST['id_log'],
                'fechareg' => $fechareg
              );

              $guardar_residuos = ModeloApartar::guardarResiduosApartarModel($datos_apartado_residuo);
              $guardar_residuos_log = ModeloApartar::apartadoResiduosLogModel($datos_apartado_residuo);
            }
          }


          if ($guardar_residuos == TRUE) {
            echo '
              <script>
              ohSnap("Confirmado correctamente!", {color: "green", "duration": "1000"});
              setTimeout(recargarPagina,1050);

              function recargarPagina(){
                window.location.replace("index");
              }
              </script>
              ';
          } else {
            echo '
              <script>
              ohSnap("Residuos no actualizados", {color: "red", "duration": "1000"});
              </script>
              ';
          }
        }
      } else {
        echo '
            <script>
            ohSnap("Reserva no actualizada", {color: "red", "duration": "1000"});
            </script>
            ';
      }
    } else {
      echo '
          <script>
          ohSnap("Ha ocurrido un error!", {color: "red", "duration": "1000"});
          </script>
          ';
    }
  }
}


function guardarDocumentoControl($datos)
{
  $nom_arch = $datos['archivo']; //obtener el nombre del archivo
  //extraer la extencion del archivo de el archivo
  $ext_arch = explode(".", $nom_arch);
  $ext_arch = end($ext_arch);
  $fecha_arch = date('YmdHis');

  $nombre_archivo = strtolower(md5($datos['usuario'] . '_' . $fecha_arch)) . '.' . $ext_arch;

  $datos_hoja = array(
    'nombre' => $nombre_archivo,
    'id_user' => $datos['usuario'],
    'id_log' => $datos['id_user'],
    'id_apartado' => $datos['id_reserva'],
    'tipo' => $datos['tipo'],
    'fechareg' => $datos['fechareg']
  );

  $guardar_hoja = ModeloApartar::guardarDocumentosModel($datos_hoja);

  if ($guardar_hoja == TRUE) {
    //ruta donde de alojamiento el archivo
    $carp_destino = PUBLIC_PATH_ARCH . 'upload' . DS;
    $ruta_img = $carp_destino . $nombre_archivo;

    //verificar si subio el archivo y se mueve a su destino
    if (is_uploaded_file($_FILES['' . $datos['variable'] . '']['tmp_name'])) {
      move_uploaded_file($_FILES['' . $datos['variable'] . '']['tmp_name'], $ruta_img);
    }

    return TRUE;
  }
}
