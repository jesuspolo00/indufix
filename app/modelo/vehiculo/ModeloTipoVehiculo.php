<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloTipoVehiculo extends conexion
{

	public static function registrarTipoVehiculoModelo($datos)
	{
		$tabla = 'tipo_vehiculo';
		$cnx   = conexion::singleton_conexion();
		$sql   = "INSERT INTO " . $tabla . " (nombre, estado) VALUES (:n, :e)";
		try {
			$preparado = $cnx->preparar($sql);
			$preparado->bindParam(':n', $datos['nombre']);
			$preparado->bindValue(':e', 'activo');
			if ($preparado->execute()) {
				return true;
			} else {
				return false;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}

	public static function editarTipoVehiculoModelo($datos)
	{
		$tabla = 'tipo_vehiculo';
		$cnx   = conexion::singleton_conexion();
		$sql   = "UPDATE " . $tabla . " SET nombre = :n WHERE id_tipo = :id";
		try {
			$preparado = $cnx->preparar($sql);
			$preparado->bindParam(':n', $datos['nombre']);
			$preparado->bindValue(':id', (int) trim($datos['id_tipo']), PDO::PARAM_INT);
			if ($preparado->execute()) {
				return true;
			} else {
				return false;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}

	public static function mostrarTipoVehiculoModelo($super_empresa)
	{
		$tabla  = 'tipo_vehiculo';
		$cnx    = conexion::singleton_conexion();
		$cmdsql = "SELECT * FROM " . $tabla . " WHERE id_super_empresa = :is";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
			$preparado->setFetchMode(PDO::FETCH_ASSOC);
			if ($preparado->execute()) {
				return $preparado->fetchAll();
			} else {
				return false;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}

	public static function mostrarTipoVehiculoIdModelo($id, $super_empresa)
	{
		$tabla  = 'tipo_vehiculo';
		$cnx    = conexion::singleton_conexion();
		$cmdsql = "SELECT * FROM " . $tabla . " WHERE id_tipo = :id AND id_super_empresa = :is";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
			$preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
            //$preparado->setFetchMode(PDO::FETCH_ASSOC);
			if ($preparado->execute()) {
				return $preparado->fetch();
			} else {
				return false;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}

	public static function inactivarTipoVehiculoModelo($id)
	{
		$tabla  = 'tipo_vehiculo';
		$cnx    = conexion::singleton_conexion();
		$cmdsql = "UPDATE " . $tabla . " SET estado = 'inactivo' WHERE id_tipo = :id";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
			$preparado->setFetchMode(PDO::FETCH_ASSOC);
			if ($preparado->execute()) {
				return true;
			} else {
				return false;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}

	public static function activarTipoVehiculoModelo($id)
	{
		$tabla  = 'tipo_vehiculo';
		$cnx    = conexion::singleton_conexion();
		$cmdsql = "UPDATE " . $tabla . " SET estado = 'activo' WHERE id_tipo = :id";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
			$preparado->setFetchMode(PDO::FETCH_ASSOC);
			if ($preparado->execute()) {
				return true;
			} else {
				return false;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}

	public static function eliminarTipoVehiculoModelo($id)
	{
		$tabla  = 'tipo_vehiculo';
		$cnx    = conexion::singleton_conexion();
		$cmdsql = "DELETE FROM " . $tabla . " WHERE id_tipo = :id";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
			$preparado->setFetchMode(PDO::FETCH_ASSOC);
			if ($preparado->execute()) {
				return true;
			} else {
				return false;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}
}
