<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloHojaVida extends conexion
{


    public function registrarHojaVidaModel($datos)
    {
        $tabla = 'hoja_vida';
        $cnx = conexion::singleton_conexion();
        $sql = "INSERT INTO " . $tabla . " (id_vehiculo, mantenimiento, fecha_adquisicion, fecha_vence, cont_garantia, fechareg, fecha_update, id_log) VALUES (:iv,:m,:fa,:fv,:cg,:fr,:fu,:il)";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':iv', $datos['id_vehiculo']);
            $preparado->bindParam(':m', $datos['mantenimiento']);
            $preparado->bindParam(':fa', $datos['fecha_adquisicion']);
            $preparado->bindParam(':fv', $datos['fecha_vence']);
            $preparado->bindValue(':cg', '');
            $preparado->bindParam(':fr', $datos['fechareg']);
            $preparado->bindParam(':fu', $datos['fecha_update']);
            $preparado->bindParam(':il', $datos['id_log']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function actualizarHojaVidaModel($datos)
    {
        $tabla = 'hoja_vida';
        $cnx = conexion::singleton_conexion();
        $sql = "UPDATE " . $tabla . " SET mantenimiento = :m, fecha_adquisicion = :fa, fecha_vence = :fv, cont_garantia = :cg, fecha_update = :fu, id_log = :il WHERE id_vehiculo = :id";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':m', $datos['mantenimiento']);
            $preparado->bindParam(':fa', $datos['fecha_adquisicion']);
            $preparado->bindParam(':fv', $datos['fecha_vence']);
            $preparado->bindParam(':cg', $datos['cont_garantia']);
            $preparado->bindParam(':fu', $datos['fecha_update']);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindValue(':id', (int) trim($datos['id_vehiculo']), PDO::PARAM_INT);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function registrarComponenteHojaVidaModel($datos)
    {
        $tabla = 'componente';
        $cnx = conexion::singleton_conexion();
        $sql = "INSERT INTO " . $tabla . " (id_vehiculo, descripcion, fabricante, licencia, fecha, fechareg, id_log) VALUES (:iv,:d,:f,:l,:fe,:fr,:il)";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':iv', $datos['id_vehiculo']);
            $preparado->bindParam(':d', $datos['descripcion']);
            $preparado->bindParam(':f', $datos['fabricante']);
            $preparado->bindParam(':l', $datos['licencia']);
            $preparado->bindParam(':fe', $datos['fecha']);
            $preparado->bindParam(':fr', $datos['fechareg']);
            $preparado->bindParam(':il', $datos['id_log']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarComponenteVehiculoModel($id)
    {
        $tabla = 'componente';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_vehiculo = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }




    public function mostrarHojaVidaVehiculoModel($id)
    {
        $tabla = 'hoja_vida';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_vehiculo = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
