<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloConciliacion extends conexion
{

    public function consiliarReservaModel($datos)
    {
        $tabla = 'apartados_residuos';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_apartado, id_residuo, id_tipo_residuo, cantidad, id_td1, id_td2, kilogramos, galones, id_log, id_td3, id_td4)
        VALUES (:ia,:ir,:itr,:c,:idt,:idd,:k,:g,:il,:ide,:idf);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ia', $datos['id_apartado']);
            $preparado->bindParam(':ir', $datos['id_residuo']);
            $preparado->bindParam(':itr', $datos['id_tipo_residuo']);
            $preparado->bindParam(':c', $datos['cantidad']);
            $preparado->bindParam(':idt', $datos['id_td1']);
            $preparado->bindParam(':idd', $datos['id_td2']);
            $preparado->bindParam(':ide', $datos['id_td3']);
            $preparado->bindParam(':idf', $datos['id_td4']);
            $preparado->bindParam(':k', $datos['kilogramos']);
            $preparado->bindParam(':g', $datos['galones']);
            $preparado->bindParam(':il', $datos['id_log']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function consiliarReservaLogModel($datos)
    {
        $tabla = 'apartados_residuos_log';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_apartado, id_residuo, id_tipo_residuo, cantidad, id_td1, id_td2, kilogramos, galones, id_log, id_td3, id_td4)
        VALUES (:ia,:ir,:itr,:c,:idt,:idd,:k,:g,:il,:ide,:idf);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ia', $datos['id_apartado']);
            $preparado->bindParam(':ir', $datos['id_residuo']);
            $preparado->bindParam(':itr', $datos['id_tipo_residuo']);
            $preparado->bindParam(':c', $datos['cantidad']);
            $preparado->bindParam(':idt', $datos['id_td1']);
            $preparado->bindParam(':idd', $datos['id_td2']);
            $preparado->bindParam(':ide', $datos['id_td3']);
            $preparado->bindParam(':idf', $datos['id_td4']);
            $preparado->bindParam(':k', $datos['kilogramos']);
            $preparado->bindParam(':g', $datos['galones']);
            $preparado->bindParam(':il', $datos['id_log']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function consiliarReservaSiModel($id)
    {
        $tabla = 'reservas';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET conciliacion = 'si' WHERE id_reserva = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', $id);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
