<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloTipoCategorias extends conexion
{

    public function guardarTipoCategoriaModel($datos)
    {
        $tabla = 'td_tipo';
        $cnx = conexion::singleton_conexion();
        $sql = "INSERT INTO " . $tabla . " (nombre, id_log, id_categoria, id_super_empresa) VALUES (:n, :ul, :ic, :id)";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':ul', $datos['user_log']);
            $preparado->bindParam(':ic', $datos['categoria']);
            $preparado->bindParam(':id', $datos['super_empresa']);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function editarTipoCategoriaModel($datos)
    {
        $tabla = 'td_tipo';
        $cnx = conexion::singleton_conexion();
        $sql = "UPDATE " . $tabla . " SET nombre = :n WHERE id = :id";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindValue(':id', (int) trim($datos['id']), PDO::PARAM_INT);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarTipoDestinoModel($super_empresa)
    {
        $tabla = 'td_tipo';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_super_empresa = :is AND id_categoria = 2";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }




    public function mostrarListadoTratamientoModel($super_empresa)
    {
        $tabla = 'td_listado';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM td_listado WHERE activo = 1 AND id_super_empresa = '$super_empresa'
        AND id_td_tipo IN(SELECT td.id FROM td_tipo td WHERE td.`nombre` LIKE 'TRATAMIENTO%');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarListadoDisposicionModel($super_empresa)
    {
        $tabla = 'td_listado';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM td_listado WHERE activo = 1 AND id_super_empresa = '$super_empresa'
        AND id_td_tipo IN(SELECT td.id FROM td_tipo td WHERE td.`nombre` LIKE 'DISPOSICION FINAL%');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarListadoDestinoModel($super_empresa)
    {
        $tabla = 'td_listado';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM td_listado WHERE activo = 1 AND id_super_empresa = '$super_empresa'
        AND id_td_tipo IN(SELECT td.id FROM td_tipo td WHERE td.`nombre` IN('DESTINO'));";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarListadoDestinoFinalModel($super_empresa)
    {
        $tabla = 'td_listado';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM td_listado WHERE activo = 1 AND id_super_empresa = '$super_empresa'
        AND id_td_tipo IN(SELECT td.id FROM td_tipo td WHERE td.`nombre` LIKE 'DESTINO FINAL%');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function mostrarTipoTratamientoModel($super_empresa)
    {
        $tabla = 'td_tipo';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_super_empresa = :is AND id_categoria = 1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function mostrarTipoCategoriaIdModel($id)
    {
        $tabla = 'td_tipo';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function inactivarTipoCategoriaModel($id)
    {
        $tabla = 'td_tipo';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 0 WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }



    public function activarTipoCategoriaModel($id)
    {
        $tabla = 'td_tipo';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 1 WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }


    public function eliminarTipoCategoriaModel($id)
    {
        $tabla = 'td_tipo';
        $cnx = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return TRUE;
            } else {
                return FALSE;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
