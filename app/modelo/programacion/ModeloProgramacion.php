<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloProgramacion extends conexion
{

    public static function mostrarProgramacionModel()
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT r.*, (SELECT e.ciudad from empresas e where e.id_empresa = r.id_empresa) as ciudades FROM reservas r group by r.fecha_apartado, r.id_vehiculo order by r.fecha_apartado desc";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarProgramacionIdModel($id)
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_reserva = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function consultarPrefacturaModel($id)
    {
        $tabla  = 'prefacturacion';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT id FROM " . $tabla . " WHERE id_reserva = :id ORDER BY id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarConsecutivoModel($datos)
    {
        $tabla  = 'consecutivo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_reserva, numero, fechareg, id_log) VALUES (:ir,:n,:fr,:il)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ir', $datos['id_reserva']);
            $preparado->bindParam(':n', $datos['numero']);
            $preparado->bindParam(':fr', $datos['fechareg']);
            $preparado->bindParam(':il', $datos['id_log']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarConsecutivoCertModel($datos)
    {
        $tabla  = 'consecutivo_cert';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_reserva, numero, fechareg, id_log, nota) VALUES (:ir,:n,:fr,:il,:nt)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ir', $datos['id_reserva']);
            $preparado->bindParam(':n', $datos['numero']);
            $preparado->bindParam(':fr', $datos['fechareg']);
            $preparado->bindParam(':il', $datos['id_log']);
            $preparado->bindParam(':nt', $datos['nota']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarConsecutivoReservaModel($id)
    {
        $tabla  = 'consecutivo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_reserva = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarConsecutivoCertReservaModel($id)
    {
        $tabla  = 'consecutivo_cert';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_reserva = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function validarConsecutivoReservaModel($numero)
    {
        $tabla  = 'consecutivo';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE numero = :n";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $numero);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function validarConsecutivoCertReservaModel($numero)
    {
        $tabla  = 'consecutivo_cert';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE numero = :n";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $numero);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarResiduosProgramacionModel($id)
    {
        $tabla  = 'apartados_residuos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        ap.id_apartado,
        (SELECT CONCAT('CM ' , m.numero, ' ', DATE(
        (SELECT r.fechareg FROM reservas r WHERE r.id_reserva = ap.id_apartado)
        )) FROM consecutivo m WHERE m.id_reserva = ap.id_apartado) AS servicio,
        (SELECT CONCAT('CM ' , m.numero, ' ', DATE(
        (SELECT r.fechareg FROM reservas r WHERE r.id_reserva = ap.id_apartado)
        )) FROM consecutivo_cert m WHERE m.id_reserva = ap.id_apartado) AS servicio_recepcion,
        (SELECT r.id_empresa FROM reservas r WHERE r.id_reserva = ap.id_apartado) AS id_empresa,
        (SELECT (SELECT e.nombre FROM empresas e WHERE e.id_empresa = r.id_empresa) FROM reservas r WHERE r.id_reserva = ap.id_apartado) AS empresa,
        (SELECT IF(r.id_sucursal <> 0, (SELECT e.ciudad FROM sucursales e WHERE e.id_sucursal = r.id_sucursal),
        (SELECT e.ciudad FROM empresas e WHERE e.id_empresa = r.id_empresa)) FROM reservas r WHERE r.id_reserva = ap.id_apartado) AS ciudad,
        (SELECT re.nombre FROM residuos re WHERE re.id_residuo = ap.id_residuo) AS residuo,
        (SELECT re.codigo FROM residuos re WHERE re.id_residuo = ap.id_residuo) AS codigo_residuo,
        ap.kilogramos,
        ap.galones,
        (SELECT td.nombre FROM td_listado td WHERE td.id = ap.id_td1) AS tratamiento,
        (SELECT td.nombre FROM td_listado td WHERE td.id = ap.id_td2) AS disposicion,
        (SELECT td.nombre FROM td_listado td WHERE td.id = ap.id_td3) AS destino,
        (SELECT td.nombre FROM td_listado td WHERE td.id = ap.id_td4) AS destino_final,
        (SELECT r.recepcion FROM reservas r WHERE r.id_reserva = ap.id_apartado) AS recepcion,
        ap.id_residuo,
        ap.id_tipo_residuo,
        ap.cantidad,
        ap.fechareg
        FROM " . $tabla . " ap WHERE ap.id_apartado = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarResiduosTipoProgramacionModel($id, $tipo)
    {
        $tabla  = 'apartados_residuos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_apartado = :id AND id_residuo = :t";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->bindValue(':t', (int) trim($tipo), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarReservaVehiculoModel($sql, $fecha_ini, $fecha_fin)
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        r.id_reserva,
        (SELECT e.nombre FROM empresas e WHERE e.id_empresa IN(r.id_empresa)) AS empresa,
        (SELECT e.nombre FROM sucursales e WHERE e.id_sucursal IN(r.id_sucursal)) AS sucursal,
        IF(r.id_sucursal = 0,
        (SELECT e.direccion FROM empresas e WHERE e.id_empresa IN(r.id_empresa)),
        (SELECT s.direccion FROM sucursales s WHERE s.id_sucursal IN(r.id_sucursal))) AS direccion,
        IF(r.id_sucursal = 0,
        (SELECT e.telefono FROM empresas e WHERE e.id_empresa IN(r.id_empresa)),
        (SELECT s.telefono FROM sucursales s WHERE s.id_sucursal IN(r.id_sucursal))) AS telefono,
        IF(r.id_sucursal = 0,
        (SELECT e.nom_contacto FROM empresas e WHERE e.id_empresa IN(r.id_empresa)),
        (SELECT s.nom_contacto FROM sucursales s WHERE s.id_sucursal IN(r.id_sucursal))) AS contacto,
        IF(r.id_sucursal = 0,
        (SELECT e.ciudad FROM empresas e WHERE e.id_empresa IN(r.id_empresa)),
        (SELECT s.ciudad FROM sucursales s WHERE s.id_sucursal IN(r.id_sucursal))) AS ciudad,
        r.fecha_apartado,
        r.hora_inicio,
        r.hora_fin,
        r.observacion,
        r.recepcion,
        r.confirmado,
        r.conciliacion,
        r.id_vehiculo,
        IF(r.recepcion = 1, (SELECT re.placa_vehiculo FROM recepcion re WHERE re.id_apartado IN(r.id_reserva)),
        (SELECT i.placa FROM inventario i WHERE i.id_inventario IN(r.id_vehiculo))) AS vehiculo,
        IF(r.recepcion = 1 , (SELECT re.nom_conductor FROM recepcion re WHERE re.id_apartado IN(r.id_reserva)),
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(r.id_conductor))) AS conductor,
        IF(r.recepcion = 1 , (SELECT re.nom_coopiloto FROM recepcion re WHERE re.id_apartado IN(r.id_reserva)),
        (SELECT CONCAT(u.nombre, ' ', u.apellido) FROM usuarios u WHERE u.id_user IN(r.id_coopiloto))) AS coopiloto
        FROM " . $tabla . " r WHERE r.fechareg BETWEEN '" . $fecha_ini . " 00:00:00' AND '" . $fecha_fin . " 23:59:59'" . $sql;
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarReservaFechaModel($fecha_ini, $fecha_fin)
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " r WHERE r.fechareg BETWEEN '" . $fecha_ini . " 00:00:00' AND '" . $fecha_fin . " 23:59:59'
        AND r.id_reserva IN(SELECT a.id_apartado FROM apartados_residuos a WHERE a.id_tipo_residuo
        IN(SELECT m.id_tipo FROM tipo_residuo m WHERE m.estado IN('activo')))";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarReservaRecepcionFechaModel($fecha_ini, $fecha_fin)
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " r WHERE r.fechareg BETWEEN '" . $fecha_ini . " 00:00:00' AND '" . $fecha_fin . " 23:59:59'
        AND r.id_reserva IN(SELECT a.id_apartado FROM apartados_residuos a WHERE a.id_tipo_residuo
        IN(SELECT m.id_tipo FROM tipo_residuo m WHERE m.estado IN('activo'))) AND r.recepcion = 1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarTipoResiduoReservaModel($id)
    {
        $tabla  = 'tipo_residuo';
        $cnx    = conexion::singleton_conexion($id);
        $cmdsql = "SELECT t.* FROM " . $tabla . " t WHERE t.id_tipo in( SELECT r.id_tipo_residuo FROM residuos r WHERE r.id_residuo in(SELECT p.id_residuo FROM apartados_residuos p WHERE p.id_apartado = :id))";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTipoResiduoReservaControl($id)
    {
        $tabla  = 'apartados_residuos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " a WHERE a.id_apartado IN(" . $id . ")";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarCantidadResiduosReservaModel($id)
    {
        $tabla  = 'apartados_residuos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT *, SUM(cantidad) AS valor_total FROM " . $tabla . " WHERE id_apartado = " . $id . " GROUP BY id_tipo_residuo";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function sumarCantidadResiduosReservaModel($id)
    {
        $tabla  = 'apartados_residuos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SUM(cantidad) AS valor_total FROM " . $tabla . " WHERE id_apartado = " . $id;
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarApartadoResiduosReservaControl($id)
    {
        $tabla  = 'apartados_residuos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        r.id_apartado,
        r.id_residuo,
        (SELECT re.nombre FROM residuos re WHERE re.id_residuo = r.id_residuo) AS residuo,
        r.id_tipo_residuo,
        (SELECT tr.nombre FROM tipo_residuo tr WHERE tr.id_tipo = r.id_tipo_residuo) AS tipo_residuo,
        r.cantidad AS cantidad_confirmada,
        (SELECT cr.cantidad FROM apartados_residuos_log cr WHERE cr.id_apartado = r.id_apartado AND cr.id_residuo = r.id_residuo
        AND cr.id_log NOT IN(SELECT u.id_user FROM usuarios u WHERE u.perfil IN(5))) AS cantidad_reservada,
        r.id_log,
        (SELECT cr.id_log FROM apartados_residuos_log cr WHERE cr.id_apartado = r.id_apartado AND cr.id_residuo = r.id_residuo
        AND cr.id_log NOT IN(SELECT u.id_user FROM usuarios u WHERE u.perfil IN(5))) AS id_user_reserva,
        (SELECT (SELECT CONCAT(u.nombre, ' ' , u.apellido) FROM usuarios u WHERE u.id_user = cr.id_log) FROM apartados_residuos_log cr WHERE cr.id_apartado = r.id_apartado AND cr.id_residuo = r.id_residuo
        AND cr.id_log NOT IN(SELECT u.id_user FROM usuarios u WHERE u.perfil IN(5))) AS usuario_reserva,
        (SELECT CONCAT(u.nombre, ' ' , u.apellido) FROM usuarios u WHERE u.id_user = r.id_log) AS usuario_confirma,
        (SELECT re.recepcion FROM reservas re WHERE re.id_reserva = r.id_apartado) AS recepcion
        FROM " . $tabla . " r WHERE r.id_apartado = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function certificadosSolicitadosModel()
    {
        $tabla  = 'solicitud_certificados';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        s.id,
        if(s.id_sucursal = 0, (SELECT e.nombre from empresas e where e.id_empresa in(s.id_empresa)),
        (SELECT su.nombre from sucursales su where su.id_sucursal in(s.id_sucursal))) as empresa,
        IF(s.id_sucursal = 0, (SELECT e.ciudad FROM empresas e WHERE e.id_empresa IN(s.id_empresa)),
        (SELECT su.direccion FROM sucursales su WHERE su.id_sucursal IN(s.id_sucursal))) AS ciudad,
        IF(s.id_sucursal = 0, (SELECT e.ciudad FROM empresas e WHERE e.id_empresa IN(s.id_empresa)),
        (SELECT su.direccion FROM sucursales su WHERE su.id_sucursal IN(s.id_sucursal))) AS direccion,
        (SELECT i.placa from inventario i where i.id_inventario in(SELECT r.id_vehiculo from reservas r where r.id_reserva in(s.id_reserva))) as vehiculo,
        (SELECT CONCAT(i.nombre, ' ', i.apellido) FROM usuarios i WHERE i.id_user IN(SELECT r.id_conductor FROM reservas r WHERE r.id_reserva IN(s.id_reserva))) AS usuario,
        s.id_empresa,
        s.id_reserva,
        s.id_sucursal,
        s.id_user_solicita,
        s.estado
        from " . $tabla . " s ORDER BY s.estado;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function solicitarCertficadoModel($datos)
    {
        $tabla  = 'solicitud_certificados';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_empresa, id_sucursal, id_reserva, id_user_solicita) VALUES (:ie, :is, :ir, :iu)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ie', $datos['id_empresa']);
            $preparado->bindParam(':is', $datos['id_sucursal']);
            $preparado->bindParam(':ir', $datos['id_reserva']);
            $preparado->bindParam(':iu', $datos['id_user']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function confirmarCertificadoModel($datos)
    {
        $tabla  = 'solicitud_certificados';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET id_user_confirma = :ic, estado = 2 WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ic', $datos['id_confirma']);
            $preparado->bindParam(':id', $datos['id_solicitud']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
