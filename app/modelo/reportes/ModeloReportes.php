<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloReportes extends conexion
{


	public function MostrarReportesDanoModelo($super_empresa)
	{
		$tabla = 'reportes';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "SELECT * FROM " . $tabla . " WHERE estado IN('abierto') AND tipo_mant = 1 AND id_super_empresa = :is";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
			$preparado->setFetchMode(PDO::FETCH_ASSOC);
			if ($preparado->execute()) {
				return $preparado->fetchAll();
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}


	public function MostrarReportesMantModelo($super_empresa)
	{
		$tabla = 'reportes';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "SELECT * FROM " . $tabla . " WHERE estado IN('abierto') AND tipo_mant = 2 AND id_super_empresa = :is";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
			$preparado->setFetchMode(PDO::FETCH_ASSOC);
			if ($preparado->execute()) {
				return $preparado->fetchAll();
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}



	public function MostrarReportesOtroModelo($super_empresa)
	{
		$tabla = 'reportes';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "SELECT * FROM " . $tabla . " WHERE estado IN('abierto') AND tipo_mant = 3 AND id_super_empresa = :is";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
			$preparado->setFetchMode(PDO::FETCH_ASSOC);
			if ($preparado->execute()) {
				return $preparado->fetchAll();
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}


	public function MostrarReportesIdModel($id,$super_empresa)
	{
		$tabla = 'reportes';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "SELECT * FROM " . $tabla . " WHERE id_reporte = (SELECT MAX(m.id_reporte) FROM reportes m WHERE m.id_inventario = :id 
		AND m.id_super_empresa = :is)";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
			$preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
			$preparado->setFetchMode(PDO::FETCH_ASSOC);
			if ($preparado->execute()) {
				return $preparado->fetch();
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}


	public function mostrarTipoEstadoIdModel($id)
	{
		$tabla = 'estados';
		$cnx = conexion::singleton_conexion();
		$cmdsql = "SELECT * FROM " . $tabla . " WHERE id_estado = :id";
		try {
			$preparado = $cnx->preparar($cmdsql);
			$preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
			$preparado->setFetchMode(PDO::FETCH_ASSOC);
			if ($preparado->execute()) {
				return $preparado->fetch();
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}


	public function reportarSolucionModelo($datos)
	{
		$tabla = 'reportes';
		$cnx = conexion::singleton_conexion();
		$sql = "UPDATE " . $tabla . " SET observacion = :o,fecha_fin_reporte = :ff, id_resp = :ir,estado = :e WHERE id_reporte = :id";
		try {
			$preparado = $cnx->preparar($sql);
			$preparado->bindParam(':o', $datos['observacion']);
			$preparado->bindParam(':ff', $datos['fecha_fin_reporte']);
			$preparado->bindParam(':ir', $datos['id_resp']);
			$preparado->bindValue(':e', 'cerrado');
			$preparado->bindValue(':id', (int) trim($datos['id_reporte']), PDO::PARAM_INT);
			if ($preparado->execute()) {
				$id = $cnx->ultimoIngreso($tabla);
				$resultado = array('solucion' => TRUE, 'id' => $datos['id_reporte']);
				return $resultado;
			} else {
				return FALSE;
			}
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage();
		}
		$cnx->closed();
		$cnx = null;
	}
}
