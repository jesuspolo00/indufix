<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloEmpresa extends conexion
{

    public static function guardarEmpresaModel($datos)
    {
        $tabla  = 'empresas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, ciudad, direccion, nom_contacto, telefono, nit, tiempo, email, id_user, id_super_empresa, estado, fechareg, fechaupdate)
        VALUES (:n,:c,:d,:nc,:t,:nt,:ti,:e,:id,:is,:es,:f,:fu)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':c', $datos['ciudad']);
            $preparado->bindParam(':d', $datos['direccion']);
            $preparado->bindParam(':nc', $datos['nom_contacto']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':nt', $datos['nit']);
            $preparado->bindParam(':ti', $datos['tiempo']);
            $preparado->bindParam(':e', $datos['email']);
            $preparado->bindParam(':id', $datos['id_user']);
            $preparado->bindParam(':is', $datos['super_empresa']);
            $preparado->bindValue(':es', 'activo');
            $preparado->bindParam(':f', $datos['fechareg']);
            $preparado->bindParam(':fu', $datos['fechaupdate']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarEmnpresaModelo($datos)
    {
        $tabla = 'empresas';
        $cnx   = conexion::singleton_conexion();
        $sql   = "UPDATE " . $tabla . " SET nombre = :n, ciudad = :c, direccion = :d, nom_contacto = :nc, telefono = :t, nit = :nt, tiempo = :ti, email = :e, id_user = :id, fechaupdate = :fu WHERE id_empresa = :id_e";
        try {
            $preparado = $cnx->preparar($sql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':c', $datos['ciudad']);
            $preparado->bindParam(':d', $datos['direccion']);
            $preparado->bindParam(':nc', $datos['nom_contacto']);
            $preparado->bindParam(':t', $datos['telefono']);
            $preparado->bindParam(':nt', $datos['nit']);
            $preparado->bindParam(':ti', $datos['tiempo']);
            $preparado->bindParam(':e', $datos['email']);
            $preparado->bindParam(':id', $datos['id_user']);
            $preparado->bindParam(':fu', $datos['fechaupdate']);
            $preparado->bindValue(':id_e', (int) trim($datos['id_empresa']), PDO::PARAM_INT);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarEmpresasModel($super_empresa)
    {
        $tabla  = 'empresas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_super_empresa = :is";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':is', (int) trim($super_empresa), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarEmpresaIdModel($id)
    {
        $tabla  = 'empresas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id_empresa = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function inactivarEmpresaModelo($id)
    {
        $tabla  = 'empresas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 'inactivo' WHERE id_empresa = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function activarEmpresaModelo($id)
    {
        $tabla  = 'empresas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET estado = 'activo' WHERE id_empresa = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function eliminarEmpresaModelo($id)
    {
        $tabla  = 'empresas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "DELETE FROM " . $tabla . " WHERE id_empresa = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarCiudadEmpresaModel($id)
    {
        $tabla  = 'empresas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT ciudad FROM " . $tabla . " WHERE id_empresa = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindValue(':id', (int) trim($id), PDO::PARAM_INT);
            $preparado->setFetchMode(PDO::FETCH_ASSOC);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
