<?php
date_default_timezone_set('America/Bogota');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlTipoVehiculo.php';

$objetClass = ControlTipoVehiculo::singleton_tipo_vehiculo();
$rs = $objetClass->activarTipoVehiculoControl();
echo $rs;
?>