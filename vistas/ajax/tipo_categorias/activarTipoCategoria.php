<?php
date_default_timezone_set('America/Bogota');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'categorias' . DS . 'ControlTipoCategorias.php';

$objetClass = ControlTipoCategorias::singleton_tipo_categorias();
$rs = $objetClass->activarTipoCategoriaControl();
echo $rs;
