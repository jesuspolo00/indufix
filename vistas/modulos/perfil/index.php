<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
include_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';

$instancia = ControlPerfil::singleton_perfil();

$datos        = $instancia->mostrarDatosPerfilControl($id_log, $id_super_empresa);
$datos_perfil = $instancia->mostrarPerfilesControl($id_super_empresa);
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3">
					<h4 class="m-0 font-weight-bold text-orange">Perfil</h4>
				</div>
				<div class="card-body">
					<form action="" method="POST" id="form_enviar">
						<input type="hidden" value="<?=$id_log?>" name="id_user">
						<input type="hidden" value="<?=$datos['pass']?>" name="pass_old">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label class="font-weight-bold">Numero de Documento <span class="text-danger">*</span></label>
									<input type="text" class="form-control numeros" maxlength="50" minlength="1" value="<?=$datos['documento']?>" readonly name="documento">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
									<input type="text" class="form-control letras" maxlength="50" minlength="1" value="<?=$datos['nombre']?>" name="nombre">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="font-weight-bold">Apellido</label>
									<input type="text" class="form-control letras" maxlength="50" minlength="1" value="<?=$datos['apellido']?>" name="apellido">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="font-weight-bold">Correo <span class="text-danger">*</span></label>
									<input type="email" class="form-control" maxlength="50" minlength="1" value="<?=$datos['correo']?>" name="correo" readonly>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="font-weight-bold">Telefono</label>
									<input type="text" class="form-control numeros" maxlength="50" minlength="1" value="<?=$datos['telefono']?>" name="telefono">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="font-weight-bold">Usuario <span class="text-danger">*</span></label>
									<input type="text" class="form-control" maxlength="50" minlength="1" value="<?=$datos['user']?>" name="usuario" readonly>
								</div>
							</div>
							<div class="form-group col-lg-12 mt-4 text-center">
								<h5 class="font-weight-bold">Cambio de contrase&ntilde;a</h5>
								<hr>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="font-weight-bold">Contrase&ntilde;a</label>
									<input type="password" class="form-control" maxlength="16" minlength="8" name="password" id="password">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label class="font-weight-bold">Confirmar Contrase&ntilde;a</label>
									<input type="password" class="form-control" maxlength="16" minlength="8" name="conf_password" id="conf_password">
								</div>
							</div>
						</div>
						<div class="form-group mt-4 float-right">
							<button type="submit" class="btn btn-success btn-sm" id="enviar_perfil">
								<i class="fa fa-save"></i>
								&nbsp;
								Guardar cambios
							</button>
							<input type="hidden" name="perfil" value="<?=$datos['perfil']?>">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
if (isset($_POST['documento'])) {
    $instancia->editarPerfilControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/validaciones.js"></script>