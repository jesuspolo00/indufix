<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'programacion' . DS . 'ControlProgramacion.php';

$instancia = ControlProgramacion::singleton_programacion();

$id_super_empresa = $_SESSION['super_empresa'];

if (isset($_GET['fecha_ini'])) {

	$id_vehiculo = (!empty($_GET['vehiculo'])) ? base64_decode($_GET['vehiculo']) : 0;
	$fecha_ini = base64_decode($_GET['fecha_ini']);
	$fecha_fin = base64_decode($_GET['fecha_fin']);
	$buscar = $instancia->buscarReservaVehiculoControl($id_vehiculo, $fecha_ini, $fecha_fin);


	class MYPDF extends TCPDF
	{
		public function Header()
		{
			/* $this->setJPEGQuality(90);
			$this->Image(PUBLIC_PATH . 'img/logo.png', 20, 10, 50); */
		}

		public function Footer()
		{
			$this->SetY(-15);
			$this->SetFillColor(127);
			$this->SetTextColor(127);
			$this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
			$this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
		}
	}


	// create a PDF object
	$pdf = new MYPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document (meta) information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Jesus Polo');
	$pdf->SetTitle('Programacion Diaria');
	$pdf->SetSubject('Programacion Diaria');
	$pdf->SetKeywords('Programacion Diaria');
	$pdf->AddPage();


	$pdf->setJPEGQuality(100);
	$pdf->Image(PUBLIC_PATH . 'img/logo.png', 20, 10, 50);

	$pdf->Cell(150);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 15);
	$pdf->Cell(120, 12, 'ITINERARIO DE SERVICIOS', 0, 0, 'C');

	$pdf->Ln(6);
	$pdf->Cell(150);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 8);
	$pdf->Cell(120, 12, 'Fecha de vigencia:', 0, 0, 'C');

	$ln = 28;

	if ($id_vehiculo != 0) {
		$encabezado = '
		<table cellpadding="2" cellspacing="10" style="width: 100%; font-size: 1.3em;">
			<tr>
				<td style="width: 40%;"><strong>Fecha:</strong> ' . $fecha_ini . ' - ' . $fecha_fin . '</td>
				<td style="width: 32%;"><strong>Conductor:</strong> ' . $buscar[0]['conductor'] . '</td>
				<td style="width: 25%;"><strong>Placa:</strong> ' . $buscar[0]['vehiculo'] . '</td>
				<td style="width: 40%;"><strong>Ayudante:</strong> ' . $buscar[0]['copiloto'] . '</td>
			</tr>
		</table>
		';

		$pdf->Ln(20);
		$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
		$pdf->Cell(10);
		$pdf->writeHTMLCell(200, 0, '', '', $encabezado, '', 1, 0, true, 'L', true);

		$ln = 8;
	}


	$pdf->Ln($ln);
	$pdf->Cell(10);

	$tabla = '
	<table border="1" style="font-size:8.5px; width:96%; font-size: 1.1em; ">
	<tr style="text-align:center; font-weight:bold; text-transform: uppercase;">
		<th>VEHICULO</th>
		<th>EMPRESA</th>
		<th>SUCURSAL</th>
		<th>CIUDAD</th>
		<th>DIRECCION</th>
		<th>TELEFONO</th>
		<th>NOMBRE CONTACTO</th>
		<th>FECHA APARTADO</th>
		<th>HORA INICIO</th>
		<th>HORA FIN</th>
		<th>OBSERVACIONES</th>
	</tr>
	';

	foreach ($buscar as $datos) {

		$id_reserva = $datos['id_reserva'];
		$empresa = $datos['empresa'];
		$sucursal = $datos['sucursal'];
		$ciudad = $datos['ciudad'];
		$direccion = $datos['direccion'];
		$telefono = $datos['telefono'];
		$contacto = $datos['contacto'];
		$fecha = $datos['fecha_apartado'];
		$hora_inicio = $datos['hora_inicio'];
		$hora_fin = $datos['hora_fin'];
		$observacion = $datos['observacion'];
		$recepcion = $datos['recepcion'];
		$confirmado = $datos['confirmado'];
		$conciliacion = $datos['conciliacion'];
		$id_vehiculo = $datos['id_vehiculo'];
		$vehiculo = $datos['vehiculo'];
		$coopiloto = $datos['coopiloto'];

		$tabla .= '
			<tr>
				<td>' . $vehiculo . '</td>
				<td>' . $empresa . '</td>
				<td>' . $sucursal . '</td>
				<td>' . $ciudad . '</td>
				<td>' . $direccion . '</td>
				<td>' . $telefono . '</td>
				<td>' . $contacto . '</td>
				<td>' . $fecha . '</td>
				<td>' . $hora_inicio . '</td>
				<td>' . $hora_fin . '</td>
				<td>' . $observacion . '</td>
			</tr>
		';
	}

	$tabla .= '
	</table>
	';

	$pdf->writeHTML($tabla, true, false, true, false, '');



	$pie = '
	<table cellpadding="2" cellspacing="2" style="width: 80%; height: 200px;">
		<tr>
			<td style="width: 40%;"><strong>_________________________</strong></td>
			<td style="width: 40%;"><strong>_________________________</strong></td>
			<td style="width: 40%;"><strong>_________________________</strong></td>
			<td style="width: 40%;"><strong>_________________________</strong></td>
		</tr>
		<tr>
			<td><strong>ENTREGADO POR</strong></td>
			<td><strong>SOCIALIZADO</strong></td>
			<td><strong>RESPONSABLE DE CARGUE</strong></td>
			<td><strong>CONDUCTOR</strong></td>
		</tr>
	</table>
	';

	$pdf->Ln(20);
	$pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
	$pdf->Cell(15);
	$pdf->writeHTMLCell(200, 0, '', '', $pie, '', 1, 0, true, 'C', true);

	$pdf->Output('Programacion diaria.pdf');
}
