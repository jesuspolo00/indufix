<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'prefactura' . DS . 'ControlPrefactura.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';
require_once CONTROL_PATH . 'numeros.php';

$instancia = ControlPrefactura::singleton_prefactura();
$instancia_permisos = ControlPermiso::singleton_permiso();

$id_super_empresa = $_SESSION['super_empresa'];
$datos_super_empresa = $instancia_permisos->datosSuperEmpresaControl($id_super_empresa);

if (isset($_GET['reserva'])) {
    $id_reserva = base64_decode($_GET['reserva']);

    $datos_prefactura_def = $instancia->mostrarDatosPrefacturaControl($id_reserva);
    $datos_totales = $instancia->mostrarDatosPrefacturacionEmpresaControl($id_reserva);
    $datos_prefactura_alt = $instancia->mostrarDatosPrefacturacionAlteradaEmpresaControl($id_reserva);

    $meses = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');

    if (count($datos_prefactura_alt) < 0) {
        $datos_prefactura = $datos_prefactura_def;
    } else {
        $datos_prefactura = $datos_prefactura_alt;
    }

    $dia_letras = date('l', strtotime($datos_totales['fechareg']));
    $dias_numero = date('d', strtotime($datos_totales['fechareg']));
    $mes_letra = $meses[(date('m', strtotime($datos_totales['fechareg'])) * 1) - 1];
    $anio_numero = date('Y', strtotime($datos_totales['fechareg']));


    class MYPDF extends TCPDF
    {

        public function setData($dia_letras, $dias_numero, $mes_letra, $anio_numero)
        {
            $this->dia_letra = $dia_letras;
            $this->dia_numero = $dias_numero;
            $this->mes_letra = $mes_letra;
            $this->anio_numero = $anio_numero;
        }

        public function Header()
        {
            /* $this->setJPEGQuality(90);
            $this->Image(PUBLIC_PATH . 'img/logo.png', 15, 10, 35);
            $this->Ln(-7);
            $this->Cell(140);
            $this->SetFont(PDF_FONT_NAME_MAIN, 'B', 10);
            $this->Cell(12, 50, 'PREFACTURA DE SERVICIOS', 0, 0, 'C'); */

            // create some HTML content
            $this->Ln(8);
            $fecha = '<p style="font-size: 0.6em;">' . diaSemana($this->dia_letra) . ', ' . $this->dia_numero . ' de ' . $this->mes_letra . ' del ' . $this->anio_numero . '</p>';
            $this->writeHTMLCell(43, 0, '', '', $fecha, '', 1, 0, true, 'C', true);

            $this->Ln(0);
            $this->Cell(5);
            $html = '
                <table cellpadding="5" style="width:98%;" border="1">
                <tr style="text-align:center; font-size: 0.8em; font-weight: bold;">
                <td colspan="2" style="border:none;"><img src="' . PUBLIC_PATH . 'img/logo.png" border="0" width="100"></td>
                <td colspan="4" style="border:none;">
                PRE - FACTURA DE SERVICIOS
                </td>
                </tr>
                <tr style="text-align:center; font-size: 0.8em; border:none;">
                </tr>
                </table>';

            // output the HTML content
            $this->writeHTMLCell(185, 0, '', '', $html, '', 1, 0, true, 'C', true);
        }
    }


    $pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    $pdf->setData($dia_letras, $dias_numero, $mes_letra, $anio_numero);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Jesus Polo');
    $pdf->SetTitle('Prefactura');
    $pdf->SetSubject('Prefactura');
    $pdf->SetKeywords('Prefactura');
    $pdf->AddPage();

    $parrafo = '
    <p style="text-align:justify;">
        <strong>' . $datos_totales['nombre'] . '</strong><br>
        ' . $datos_totales['nit'] . '<br>
        ' . $datos_totales['telefono'] . '<br>
        ' . $datos_totales['direccion'] . '<br>
        ' . $datos_totales['ciudad'] . '<br>
    </p>
	';

    $pdf->Ln(30);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 8.3);
    $pdf->Cell(5);
    $pdf->writeHTMLCell(170, 0, '', '', $parrafo, '', 1, 0, true, 'C', true);

    $parrafo = '
    <p style="text-align:justify;">
        <strong>NOMBRE CLIENTE:</strong> ' . $datos_totales['empresa'] . '<br>
        <strong>NIT:</strong> ' . $datos_totales['nit_empresa'] . '<br>
        <strong>PERSONA CONTACTO:</strong> ' . $datos_totales['contacto_empresa'] . '<br>
        <strong>TELEFONO:</strong> ' . $datos_totales['telefono_empresa'] . ' <br>
        <strong>SEDE:</strong> ' . $datos_totales['sede'] . '<br>
    </p>
	';

    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 8.3);
    $pdf->Ln(-22.3);
    $pdf->Cell(115);
    $pdf->writeHTMLCell(170, 0, '', '', $parrafo, '', 1, 0, true, 'C', true);

    $tabla_ln = 10;

    $pdf->Ln($tabla_ln);
    $pdf->Cell(5);

    $tabla = '
    <table style="font-size:8.5px; width:98.3%; border-bottom: none;" cellpadding="2">
    <tr style="text-align:center; font-weight:bold;">
        <th colspan="7" style="border: 1px solid black; border-collapse: collapse;">DATOS DE LA PREFACTURA</th>    
    </tr>
    <tr style="text-align:center; font-weight:bold; border-bottom: none;">
        <th style="border: 1px solid black; border-collapse: collapse;">No. CERTIFICADO</th>
        <th style="border: 1px solid black; border-collapse: collapse;">FECHA</th>
        <th style="border: 1px solid black; border-collapse: collapse;">CLASE DE RESIDUO</th>
        <th style="border: 1px solid black; border-collapse: collapse;">CANTIDAD</th>
        <th style="border: 1px solid black; border-collapse: collapse;">UNIDAD</th>
        <th style="border: 1px solid black; border-collapse: collapse;">VR UNITARIO</th>
        <th style="border: 1px solid black; border-collapse: collapse;">VR TOTAL</th>
	</tr>
	';

    foreach ($datos_prefactura as $datos) {
        $id_residuo = $datos['id_residuo'];
        $numero_certificado = $datos['numero_certificado'];
        $residuo = $datos['residuo'];
        $kilogramos = $datos['kilogramos'];
        $galones = $datos['galones'];
        $listado = $datos['listado'];
        $valor = $datos['valor_unt'];
        $fecha_registro = $datos['fecha_registro'];
        $fecha_prefactura = $datos['fecha_prefactura'];
        $fecha_apartado = $datos['fecha_apartado'];

        $total = $kilogramos * $valor;

        $cantidad = ($galones != 0) ? $kilogramos . ' / ' . $galones : $kilogramos;
        $unidad = ($galones != 0) ? 'Kg / Gl' : 'Kg';

        $tabla .= '
        <tr style="text-align:center; font-weight:normal;">
            <td style="border: 1px solid black; border-collapse: collapse;">' . $numero_certificado . '</td>
            <td style="border: 1px solid black; border-collapse: collapse;">' . $fecha_apartado . '</td>
            <td style="border: 1px solid black; border-collapse: collapse;">' . $residuo . '</td>
            <td style="border: 1px solid black; border-collapse: collapse;">' . $cantidad . '</td>
            <td style="border: 1px solid black; border-collapse: collapse;">' . $unidad . '</td>
            <td style="border: 1px solid black; border-collapse: collapse;">$' . number_format($valor) . '</td>
            <td style="border: 1px solid black; border-collapse: collapse;">$' . number_format($total) . '</td>
        </tr>
        ';
    }

    $tabla .= '
    <tr style="font-weight:bold;">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: left; border: 1px solid black; border-collapse: collapse;">SUBTOTAL</td>
        <td style="text-align: right; border: 1px solid black; border-collapse: collapse;">$' . number_format($datos_totales['subtotal']) . '</td>
    </tr>
    <tr style="font-weight:bold;">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: left; border: 1px solid black; border-collapse: collapse;">DESCUENTO</td>
        <td style="text-align: right; border: 1px solid black; border-collapse: collapse;">' . $datos_totales['descuento'] . '%</td>
    </tr>
    <tr style="font-weight:bold;">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: left; border: 1px solid black; border-collapse: collapse;">IVA</td>
        <td style="text-align: right; border: 1px solid black; border-collapse: collapse;">' . $datos_totales['iva'] . '%</td>
    </tr>
    <tr style="font-weight:bold;">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: left; border: 1px solid black; border-collapse: collapse;">TOTAL</td>
        <td style="text-align: right; border: 1px solid black; border-collapse: collapse;">$' . number_format($datos_totales['total_final']) . '</td>
    </tr>
    ';


    $tabla .= '
	</table>
    ';

    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->writeHTML($tabla, true, false, true, false, '');


    $parrafo = '
    <p>NOTAS:</p>
    <p style="text-align: justify; font-weight: normal;">
    ' . $datos_totales['observacion'] . '
    </p>
	';

    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 8.3);
    $pdf->Ln(10);
    $pdf->Cell(5);
    $pdf->writeHTMLCell(170, 0, '', '', $parrafo, '', 1, 0, true, 'L', true);

    $ln_firma = 15;

    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 8);
    $pdf->Ln($ln_firma);
    $pdf->Cell(5);
    $pdf->Cell(120, 2, 'Elaboró:', 0, 0, 'L');

    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 11);
    $pdf->Ln($ln_firma);
    $pdf->Cell(5);
    $pdf->Cell(120, 2, '___________________________', 0, 0, 'L');

    $pdf->Ln(5);
    $pdf->Cell(5);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'IB', 8);
    $pdf->Cell(120, 2, $datos_totales['usuario'], 0, 0, 'L');

    $pdf->Ln(5);
    $pdf->Cell(5);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'I', 8);
    $pdf->Cell(120, 2, $datos_totales['perfil'], 0, 0, 'L');

    $pdf->Output('Prefactura.pdf');
}
