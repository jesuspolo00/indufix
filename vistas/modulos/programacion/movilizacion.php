<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'programacion' . DS . 'ControlProgramacion.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlEmpresa.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlSede.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlVehiculo.php';

$instancia = ControlProgramacion::singleton_programacion();
$instancia_empresa = ControlEmpresa::singleton_empresa();
$instancia_sede = ControlSede::singleton_sede();
$instancia_vehiculo = ControlVehiculo::singleton_vehiculo();

$datos_vehiculo = $instancia_vehiculo->mostrarDatosVehiculosControl();

if (isset($_GET['reserva'])) {
	$id_reserva = base64_decode($_GET['reserva']);
	$fecha_ini = base64_decode($_GET['fecha_ini']);
	$fecha_fin = base64_decode($_GET['fecha_fin']);
?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow mb-4">
					<!-- Card Header - Dropdown -->
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							<form action="<?= BASE_URL ?>programacion/index" method="POST">
								<button type="submit" class="border-0 bg-transparent">
									<i class="fa fa-arrow-left text-success"></i>
								</button>
								<input type="hidden" value="<?= $fecha_ini ?>" name="fecha_ini">
								<input type="hidden" value="<?= $fecha_fin ?>" name="fecha_fin">
								&nbsp;
								Generar Movilizacion (# <?= $id_reserva ?>)
							</form>
						</h4>
					</div>
					<div class="card-body">
						<form method="POST">
							<input type="hidden" value="<?= $_SESSION['id'] ?>" name="id_log">
							<input type="hidden" value="<?= $id_reserva ?>" name="reserva">
							<input type="hidden" value="<?= $fecha_ini ?>" name="fecha_ini">
							<input type="hidden" value="<?= $fecha_fin ?>" name="fecha_fin">
							<div class="row">
								<div class="col-lg-6 mb-2">
									<div class="form-group">
										<label>Numero de reserva</label>
										<input type="text" class="form-control numeros" disabled value="<?= $id_reserva ?>">
									</div>
								</div>
								<div class="col-lg-6 mb-2">
									<div class="form-group">
										<label>Consecutivo</label>
										<input type="text" class="form-control numeros" name="consecutivo" minlength="1" maxlength="10" required>
									</div>
								</div>
								<div class="col-lg-12 mb-2">
									<div class="form-group float-right">
										<input type="submit" value="Generar movilizacion" class="btn btn-sm btn-success">
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
	include_once VISTA_PATH . 'script_and_final.php';

	if (isset($_POST['consecutivo'])) {
		$instancia->guardarConsecutivoControl();
	}
}
?>