<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'programacion' . DS . 'ControlProgramacion.php';
require_once CONTROL_PATH . 'conciliacion' . DS . 'ControlConciliacion.php';
require_once CONTROL_PATH . 'categorias' . DS . 'ControlTipoCategorias.php';

$instancia = ControlProgramacion::singleton_programacion();
$instancia_conciliacion = ControlConciliacion::singleton_conciliacion();
$instancia_tipo_categoria = ControlTipoCategorias::singleton_tipo_categorias();

$listado_t = $instancia_tipo_categoria->mostrarListadoTratamientoControl($id_super_empresa);
$listado_d = $instancia_tipo_categoria->mostrarListadoDisposicionControl($id_super_empresa);
$listado_de = $instancia_tipo_categoria->mostrarListadoDestinoControl($id_super_empresa);
$listado_df = $instancia_tipo_categoria->mostrarListadoDestinoFinalControl($id_super_empresa);

if (isset($_GET['reserva'])) {

    $id_reserva = base64_decode($_GET['reserva']);
    $fecha_ini = base64_decode($_GET['fecha_ini']);
    $fecha_fin = base64_decode($_GET['fecha_fin']);

    $residuos = $instancia->mostrarApartadoResiduosReservaControl($id_reserva);
?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow-sm mb-4">
                    <div class="card-header py-3">
                        <h4 class="m-0 font-weight-bold text-success">
                            <form action="<?= BASE_URL ?>programacion/index" method="POST">
                                <button type="submit" class="text-decoration-none border-0 bg-transparent">
                                    <i class="fa fa-arrow-left text-success"></i>
                                </button>
                                <input type="hidden" value="<?= $fecha_ini ?>" name="fecha_ini">
                                <input type="hidden" value="<?= $fecha_fin ?>" name="fecha_fin">
                                &nbsp;
                                Conciliar Reserva (<?= $descripcion . ' - ' . $placa ?>)
                            </form>
                        </h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" class="p-2">
                            <div class="row">
                                <?php
                                foreach ($residuos as $datos) {
                                    $id_reserva = $datos['id_apartado'];
                                    $id_residuo = $datos['id_residuo'];
                                    $residuo = $datos['residuo'];
                                    $id_tipo_residuo = $datos['id_tipo_residuo'];
                                    $tipo_residuo = $datos['tipo_residuo'];
                                    $cantidad_reservada = ($datos['cantidad_reservada'] == '') ? 0 : $datos['cantidad_reservada'];
                                    $cantidad_confirmada = $datos['cantidad_confirmada'];
                                    $id_log = $datos['id_log'];
                                    $id_user_reserva = $datos['id_user_reserva'];
                                    $usuario_reserva = $datos['usuario_reserva'];
                                    $usuario_confirma = $datos['usuario_confirma'];
                                    $recepcion = $datos['recepcion'];

                                ?>
                                    <div class="col-lg-2 text-center">
                                        <div class="form-group">
                                            <h6 class="m-0 font-weight-bold text-success">Residuo</h6>
                                            <div class="input-group input-group-sm mb-3">
                                                <input type="text" maxlength="50" class="form-control" value="<?= $residuo ?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 text-center mt-d03">
                                        <div class="form-group">
                                            <h6 class="m-0 font-weight-bold text-success">Cant. Res</h6>
                                            <div class="input-group input-group-sm mb-3">
                                                <input type="text" maxlength="50" class="form-control text-center" value="<?= $cantidad_reservada ?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 text-center mt-d03">
                                        <div class="form-group">
                                            <h6 class="m-0 font-weight-bold text-success">Cant. Conf</h6>
                                            <div class="input-group input-group-sm mb-3">
                                                <input type="text" maxlength="50" class="form-control text-center" value="<?= $cantidad_confirmada ?>" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 text-center mt-d02">
                                        <div class="form-group">
                                            <div class="form-group d-inline-flex">
                                                <div class="col-lg-6">
                                                    <h6 class="m-0 font-weight-bold text-success">Cons. Kg</h6>
                                                    <div class="input-group input-group-sm mb-3">
                                                        <input type="text" class="form-control" name="kilos[]" maxlength="50" minlength="1" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <h6 class="m-0 font-weight-bold text-success">Cons. Gal</h6>
                                                    <div class="input-group input-group-sm mb-3">
                                                        <input type="text" class="form-control" name="galones[]" maxlength="50" minlength="1">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 text-center">
                                        <div class="form-group">
                                            <h6 class="m-0 font-weight-bold text-success">Destino</h6>
                                            <div class="input-group input-group-sm mb-3">
                                                <select name="destino[]" class="form-control" required>
                                                    <option value="" selected>Seleccione una opcion...</option>
                                                    <?php
                                                    foreach ($listado_de as $destino) {
                                                        $id_destino = $destino['id'];
                                                        $nombre_destino = $destino['nombre'];

                                                    ?>
                                                        <option value="<?= $id_destino ?>"><?= $nombre_destino ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <h6 class="m-0 font-weight-bold text-success">Destino Final</h6>
                                            <div class="input-group input-group-sm mb-3">
                                                <select name="destino_final[]" class="form-control" required>
                                                    <option value="" selected>Seleccione una opcion...</option>
                                                    <?php
                                                    foreach ($listado_df as $destino_final) {
                                                        $id_destino_final = $destino_final['id'];
                                                        $nombre_destino_final = $destino_final['nombre'];

                                                    ?>
                                                        <option value="<?= $id_destino_final ?>"><?= $nombre_destino_final ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <h6 class="m-0 font-weight-bold text-success">Tratamiento</h6>
                                            <div class="input-group input-group-sm mb-3">
                                                <select name="tratamiento[]" class="form-control" required>
                                                    <option value="" selected>Seleccione una opcion...</option>
                                                    <?php
                                                    foreach ($listado_t as $tratamiento) {
                                                        $id_tratamiento = $tratamiento['id'];
                                                        $nombre_tratamiento = $tratamiento['nombre'];

                                                    ?>
                                                        <option value="<?= $id_tratamiento ?>"><?= $nombre_tratamiento ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <h6 class="m-0 font-weight-bold text-success">Disposicion Final</h6>
                                            <div class="input-group input-group-sm mb-3">
                                                <select name="dispocision[]" class="form-control" required>
                                                    <option value="" selected>Seleccione una opcion...</option>
                                                    <?php
                                                    foreach ($listado_d as $disposicion) {
                                                        $id_disposicion = $disposicion['id'];
                                                        $nombre_disposicion = $disposicion['nombre'];

                                                    ?>
                                                        <option value="<?= $id_disposicion ?>"><?= $nombre_disposicion ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <br>
                                        --------------------------------------------
                                    </div>
                                    <input type="hidden" name="id_residuo[]" value="<?= $id_residuo ?>">
                                    <input type="hidden" name="id_reserva" value="<?= $id_reserva ?>">
                                    <input type="hidden" name="id_log" value="<?= $_SESSION['id'] ?>">
                                    <input type="hidden" name="id_tipo_residuo[]" value="<?= $id_tipo_residuo ?>">
                                <?php } ?>
                                <div class="col-lg-12 mt-2">
                                    <button class="btn btn-success float-right" type="submit">
                                        <i class="fas fa-check-double"></i>
                                        &nbsp;
                                        Conciliar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php

    if (isset($_POST['id_reserva'])) {
        $instancia_conciliacion->consiliarReservaControl();
    }
}
include_once VISTA_PATH . 'script_and_final.php';
