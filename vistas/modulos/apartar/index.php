<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'apartar' . DS . 'ControlApartar.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlVehiculo.php';
require_once CONTROL_PATH . 'usuario' . DS . 'ControlUsuario.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlEmpresa.php';
require_once CONTROL_PATH . 'residuos' . DS . 'ControlResiduos.php';
require_once CONTROL_PATH . 'residuos' . DS . 'ControlTipoResiduos.php';

$instancia_apartar = ControlApartar::singleton_apartar();
$instancia_tipo    = ControlTipoResiduo::singleton_tipo_residuo();
$instancia         = ControlVehiculo::singleton_vehiculo();
$instancia_residuo = ControlResiduos::singleton_residuo();
$instancia_usuario = ControlUsuario::singleton_usuario();
$instancia_empresa = ControlEmpresa::singleton_empresa();

if (isset($_GET['vehiculo'])) {
	$id_vehiculo = base64_decode($_GET['vehiculo']);

	$datos_vehiculo = $instancia->mostrarDatosVehiculosIdControl($id_vehiculo);
	$datos_tipo     = $instancia_tipo->mostrarTipoResiduoControl($id_super_empresa);
	$datos_usuario  = $instancia_usuario->mostrarDatosUsuariosControl($id_super_empresa);
	$datos_empresa  = $instancia_empresa->mostrarEmpresasControl($id_super_empresa);

	$nombre_vehiculo = $datos_vehiculo['descripcion'];
	$placa_vehiculo  = $datos_vehiculo['placa'];
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow mb-4">
					<!-- Card Header - Dropdown -->
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-orange">
							<?=$nombre_vehiculo . ' ' . $placa_vehiculo?>
						</h4>
						<div class="dropdown no-arrow">
							<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
							</a>
							<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
								<div class="dropdown-header">Acciones:</div>
								<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_vehiculo">Ver Disponibilidad</a>
							</div>
						</div>
					</div>
					<div class="card-body">
						<form method="POST" enctype="multipart/form-data">
							<input type="hidden" value="<?=$id_vehiculo?>" name="id_vehiculo">
							<input type="hidden" value="<?=$_SESSION['id']?>" name="id_log">
							<div class="row p-3">
								<div class="col-lg-6">
									<div class="form-group">
										<label class="font-weight-bold">Conductor <span class="text-danger">*</span></label>
										<select name="conductor" class="form-control" required>
											<option value="" selected>Seleccione una opcion...</option>
											<?php
											foreach ($datos_usuario as $conductor) {
												$id_conductor       = $conductor['id_user'];
												$nombre_conductor   = $conductor['nombre'];
												$apellido_conductor = $conductor['apellido'];
												$cedula_conductor   = $conductor['documento'];
												$id_perfil          = $conductor['perfil'];
												$estado             = $conductor['estado'];

												if ($id_perfil == 2 && $estado == 'activo') {
													?>
													<option value="<?=$id_conductor?>"><?=$nombre_conductor . ' ' . $apellido_conductor . ' (' . $cedula_conductor . ')'?></option>
													<?php
												}
											}
											?>
										</select>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="font-weight-bold">Coopiloto (opcional)</label>
										<select name="coopiloto" class="form-control coop">
											<option value="" selected>Seleccione una opcion...</option>
											<?php
											foreach ($datos_usuario as $coopiloto) {
												$id_coopiloto       = $coopiloto['id_user'];
												$nombre_coopiloto   = $coopiloto['nombre'];
												$apellido_coopiloto = $coopiloto['apellido'];
												$cedula_coopiloto   = $coopiloto['documento'];
												$id_perfil          = $coopiloto['perfil'];
												$estado             = $coopiloto['estado'];

												if ($id_perfil == 3 && $estado == 'activo') {
													?>
													<option value="<?=$id_coopiloto?>"><?=$nombre_coopiloto . ' ' . $apellido_coopiloto . ' (' . $cedula_coopiloto . ')'?></option>
													<?php
												}
											}
											?>
										</select>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label class="font-weight-bold">Empresa <span class="text-danger">*</span></label>
										<select name="empresa" class="form-control" id="empresa" required>
											<option value="" selected>Seleccione una opcion...</option>
											<?php
											foreach ($datos_empresa as $empresa) {
												$id_empresa     = $empresa['id_empresa'];
												$nombre_empresa = $empresa['nombre'];
												$nit            = $empresa['nit'];
												$estado         = $empresa['estado'];

												if ($estado == 'activo') {
													?>
													<option value="<?=$id_empresa?>"><?=$nombre_empresa . ' (' . $nit . ')'?></option>
													<?php
												}
											}
											?>
										</select>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="font-weight-bold">Sucursales</label>
										<select name="sucursal" disabled class="form-control" id="sucursales">
											<option value="" selected>Seleccione una opcion...</option>
										</select>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label class="font-weight-bold">Ciudad</label>
										<input type="text" disabled id="ciudad" class="form-control">
									</div>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label class="font-weight-bold">Fecha a apartar <span class="text-danger">*</span></label>
										<input type="date" name="fecha_apartar" required id="fecha_apartar" class="form-control">
									</div>
								</div>
								<div class="col-lg-6 mb-4">
									<label class="font-weight-bold">Hora inicio <span class="text-danger">*</span></label>
									<div class="input-group clock">
										<input type="time" disabled class="form-control" name="hora_inicio" required id="hora_inicio">
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-time"></span>
										</span>
									</div>
								</div>
								<div class="col-lg-6 mb-4">
									<label class="font-weight-bold">Hora fin <span class="text-danger">*</span></label>
									<div class="input-group clock">
										<input type="time" disabled name="hora_fin" required class="form-control" id="hora_fin">
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-time"></span>
										</span>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<div class="accordion" id="accordionExample">
											<?php
											$cont = 0;
											foreach ($datos_tipo as $datos) {

												$id_tipo = $datos['id_tipo'];
												$nombre  = $datos['nombre'];
												$estado  = $datos['estado'];

												$datos_residuo = $instancia_residuo->mostrarResiduosTipoIdControl($id_tipo);

												if ($estado == 'activo') {
													?>
													<div class="card border-left-danger shadow-none">
														<div class="card-header bg-white text-light" id="headingOne">
															<a href="#" class="text-decoration-none h6 mb-0 text-muted collapsed" data-toggle="collapse" data-target="#residuos<?=$id_tipo?>" aria-expanded="false" aria-controls="residuos<?=$id_tipo?>">
																<div class="row no-gutters align-items-center">
																	<div class="col mr-2">
																		<?=$nombre?>
																		<div class="h5 mb-0 font-weight-bold text-gray-800"></div>
																	</div>
																	<div class="col-auto">
																		<i class="fa fa-biohazard text-danger fa-2x cursor-pointer" data-toggle="collapse" data-target="#residuos<?=$id_tipo?>" aria-expanded="false" aria-controls="residuos<?=$id_tipo?>"></i>
																	</div>
																</div>
															</a>
														</div>
														<div id="residuos<?=$id_tipo?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
															<div class="card rounded-0">
																<div class="card-body">
																	<div class="row">
																		<?php
																		foreach ($datos_residuo as $residuos) {
																			$id_residuo     = $residuos['id_residuo'];
																			$nombre         = $residuos['nombre'];
																			$codigo         = $residuos['codigo'];
																			$estado_residuo = $residuos['estado'];

																			if ($estado_residuo == 'activo') {
																				?>
																				<div class="col-lg-4">
																					<div class="form-group">
																						<div class="custom-control custom-checkbox">
																							<input type="checkbox" class="custom-control-input check" value="<?=$id_residuo?>" id="<?=$id_residuo?>" name="id_residuo[]">
																							<label class="custom-control-label" for="<?=$id_residuo?>"><?=$nombre . ' (' . $codigo . ')'?></label>
																						</div>
																						&nbsp;
																						<div class="input-group input-group-sm">
																							<input type="number" class="form-control col-lg-4 numeros" disabled id="num_<?=$id_residuo?>" name="cantidad[]">
																						</div>
																						&nbsp;
																					</div>
																				</div>

																				<?php
																			}
																		}
																		?>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<?php
												}
											}
											?>
										</div>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label class="font-weight-bold">Observaciones</label>
										<textarea class="form-control" name="observacion" maxlength="300" minlength="1"></textarea>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label class="font-weight-bold">Hoja de vida conductor <span class="text-danger">*</span></label>
										<div class="input-group">
											<div class="custom-file">
												<input type="file" required class="custom-file-input" name="hoja_conductor" id="file" aria-describedby="inputGroupFileAddon01">
												<label class="custom-file-label" id="archivo_file" for="file"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label class="font-weight-bold">Hoja de vida coopiloto (opcional)</label>
										<div class="input-group">
											<div class="custom-file">
												<input type="file" disabled="true" class="custom-file-input" name="hoja_coopiloto" id="file_coop" aria-describedby="inputGroupFileAddon01">
												<label class="custom-file-label" id="archivo_file_coop" for="file_coop"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="form-group">
										<label class="font-weight-bold">Otros</label>
										<div class="input-group">
											<div class="custom-file">
												<input type="file" class="custom-file-input" name="otros" id="file_otros" aria-describedby="inputGroupFileAddon01">
												<label class="custom-file-label" id="archivo_file_otros" for="file_otros"></label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-12 mt-2">
									<div class="form-group float-right">
										<button class="btn btn-success btn-sm" type="submit">
											<i class="fa fa-save"></i>
											&nbsp;
											Registrar
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>




		<!--Alerta-->
		<div class="modal fade" id="alerta" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog modal-md modal-dialog-centered p-3" role="document">
				<div class="modal-content">
					<div class="modal-body border-0 text-center">
						<i class="fas fa-exclamation-triangle fa-2x text-warning"></i>
						<h4 class="text-warning">Adventercia</h4>
						<p class="mt-3">
							Le recomendamos realizar la reserva desde una computadora para que no se presenten inconvenientes.
						</p>
						<a href="<?=BASE_URL?>inicio" class="btn btn-warning btn-sm mx-auto">
							Aceptar
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php

	if (isset($_POST['conductor'])) {
		$instancia_apartar->guardarApartadoControl();
	}
}
include_once VISTA_PATH . 'script_and_final.php';
?>
<script src="<?=PUBLIC_PATH?>js/apartar/funcionesApartar.js"></script>