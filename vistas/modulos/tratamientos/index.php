<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'categorias' . DS . 'ControlListadoCategorias.php';
require_once CONTROL_PATH . 'categorias' . DS . 'ControlTipoCategorias.php';
$id_modulo = 1;

$instancia      = ControlListadoCategorias::singleton_listado_categorias();
$instancia_tipo = ControlTipoCategorias::singleton_tipo_categorias();

$datos      = $instancia->mostrarTratamientosControl($id_super_empresa);
$datos_tipo = $instancia_tipo->mostrarTipoTratamientoControl($id_super_empresa);
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-orange">
                        <a href="<?=BASE_URL?>configuracion/index" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-orange"></i>
                        </a>
                        &nbsp;
                        Tratamientos
                    </h4>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Acciones:</div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_tratamiento">Agregar Tratamiento</a>
                            <!-- <a class="dropdown-item" href="<?=BASE_URL?>tipo_tratamiento/index">Tipos de destino</a> -->
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8"></div>
                        <div class="col-lg-4">
                            <form>
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control filtro" placeholder="Buscar">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text rounded-right" id="basic-addon1">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover border table-sm" width="100%" cellspacing="0">
                                <thead>
                                    <tr class="text-center font-weight-bold">
                                        <th scope="col">#</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Tipo Tratamiento</th>
                                    </tr>
                                </thead>
                                <tbody class="buscar">
                                    <?php
                                    foreach ($datos as $destinos) {
                                        $id_destino = $destinos['id'];
                                        $nombre     = $destinos['nombre'];
                                        $nit        = $destinos['nit'];
                                        $resolucion = $destinos['resolucion'];
                                        $id_tipo    = $destinos['id_td_tipo'];
                                        $estado     = $destinos['activo'];

                                        if ($estado == 1) {
                                            $title = 'Inactivar';
                                            $icon  = '<i class="fa fa-times"></i>';
                                            $class = 'btn-danger inactivar_listado';
                                        } else {
                                            $title = 'Activar';
                                            $icon  = '<i class="fa fa-check"></i>';
                                            $class = 'btn-success activar_listado';
                                        }

                                        $tipo_tratamiento = $instancia_tipo->mostrarTipoCategoriaIdControl($id_tipo);
                                        ?>
                                        <tr class="text-center" id="destino<?=$id_destino?>">
                                            <td><?=$id_destino?></td>
                                            <td><?=$nombre?></td>
                                            <td><?=$tipo_tratamiento['nombre']?></td>
                                            <td>
                                                <div class="btn-group" role="group" aria-label="Basic example">
                                                    <button class="btn btn-sm btn-primary" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" data-target="#editar_destino<?=$id_destino?>" title="Editar destino" data-trigger="hover">
                                                        <i class="fa fa-edit"></i>
                                                    </button>

                                                    <button class="btn btn-sm <?=$class?>" data-tooltip="tooltip" data-placement="bottom" title="<?=$title?>" data-trigger="hover" id="<?=$id_destino?>">
                                                        <?=$icon?>
                                                    </button>
                                                    <button class="btn btn-sm btn-secondary eliminar_listado <?=$ver_eliminar?>" id="<?=$id_destino?>" data-toggle="tooltip" data-placement="bottom" title="Eliminar destino" data-trigger="hover">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>


                                        <!--Editar destino-->
                                        <div class="modal fade" id="editar_destino<?=$id_destino?>" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
                                            <div class="modal-dialog modal-md p-2" role="document">
                                                <div class="modal-content">
                                                    <form method="POST">
                                                        <div class="modal-header p-3">
                                                            <h4 class="modal-title text-success font-weight-bold">Ediatr destino</h4>
                                                        </div>
                                                        <div class="modal-body border-0">
                                                            <div class="row p-3">
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
                                                                        <input type="hidden" name="id_edit" value="<?=$id_destino?>">
                                                                        <input type="text" class="form-control letras" value="<?=$nombre?>" maxlength="50" minlength="1" name="descripcion_edit" required>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <div class="form-group">
                                                                        <label class="font-weight-bold">Tipo de tratamiento <span class="text-danger">*</span></label>
                                                                        <select name="id_tipo_edit" class="form-control">
                                                                            <option value="<?=$id_tipo?>" class="d-none" selected><?=$tipo_tratamiento['nombre']?></option>
                                                                            <?php
                                                                            foreach ($datos_tipo as $key) {
                                                                                $id_tipo_tratamiento = $key['id'];
                                                                                $nombre              = $key['nombre'];
                                                                                $estado              = $key['activo'];

                                                                                $ver = ($estado == 1) ? '' : 'd-none';
                                                                                ?>
                                                                                <option value="<?=$id_tipo_tratamiento?>" class="<?=$ver?>"><?=$nombre?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer border-0">
                                                            <button class="btn btn-danger" data-dismiss="modal">
                                                                <i class="fa fa-times"></i>
                                                                &nbsp;
                                                                Cancelar
                                                            </button>
                                                            <button class="btn btn-success btn-sm">
                                                                <i class="fa fa-save"></i>
                                                                &nbsp;
                                                                Guardar
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <!---------------------------------- -->
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'tratamientos' . DS . 'agregarTratamiento.php';
include_once VISTA_PATH . 'modulos' . DS . 'configuracion' . DS . 'alerta.php';

if (isset($_POST['descripcion'])) {
    $instancia->guardarListadoCategoriaControl();
}

if (isset($_POST['descripcion_edit'])) {
    $instancia->editarListadoCategoriaControl();
}
?>
<script type="text/javascript" src="<?=PUBLIC_PATH?>js/configuracion/efectos.js"></script>
<script type="text/javascript" src="<?=PUBLIC_PATH?>js/categorias/funcionesCategoria.js"></script>