<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlEmpresa.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlSede.php';
$id_modulo      = 1;
$instancia      = ControlEmpresa::singleton_empresa();
$instancia_sede = ControlSede::singleton_sede();
$datos_empresa  = $instancia->mostrarEmpresasControl($id_super_empresa);
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-orange">
						<a href="<?=BASE_URL?>configuracion/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-orange"></i>
						</a>
						&nbsp;
						Empresas
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
							<div class="dropdown-header">Acciones:</div>
							<?php
							$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 3, 6);
							if ($permiso) {
								?>
								<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_empresa">Agregar Empresa</a>
							<?php }?>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8"></div>
						<div class="col-lg-4">
							<form>
								<div class="form-group">
									<div class="input-group mb-3">
										<input type="text" class="form-control filtro" placeholder="Buscar">
										<div class="input-group-prepend">
											<span class="input-group-text rounded-right" id="basic-addon1">
												<i class="fa fa-search"></i>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="table-responsive">
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center font-weight-bold">
										<th scope="col">Nombre</th>
										<th scope="col">Ciudad</th>
										<th scope="col">Direccion</th>
										<th scope="col">Nombre contacto</th>
										<th scope="col">Telefono</th>
										<th scope="col">NIT</th>
										<th scope="col">Correo</th>
										<th scope="col">Tiempo (dias)</th>
									</tr>
								</thead>
								<tbody class="buscar">
									<?php
									foreach ($datos_empresa as $datos) {
										$id_empresa   = $datos['id_empresa'];
										$nombre       = $datos['nombre'];
										$ciudad       = $datos['ciudad'];
										$direccion    = $datos['direccion'];
										$nom_contacto = $datos['nom_contacto'];
										$nit          = $datos['nit'];
										$telefono     = $datos['telefono'];
										$tiempo       = $datos['tiempo'];
										$email        = $datos['email'];
										$estado       = $datos['estado'];

										if ($estado == 'activo') {
											$title = 'Inactivar';
											$icon  = '<i class="fa fa-times"></i>';
											$class = 'btn-danger inactivar_empresa';
										} else {
											$title = 'Activar';
											$icon  = '<i class="fa fa-check"></i>';
											$class = 'btn-success activar_empresa';
										}

										?>
										<tr class="text-center" id="empresa<?=$id_empresa;?>">
											<td><?=$nombre?></td>
											<td><?=$ciudad?></td>
											<td><?=$direccion?></td>
											<td><?=$nom_contacto?></td>
											<td><?=$telefono?></td>
											<td><?=$nit?></td>
											<td><?=$email?></td>
											<td><?=$tiempo?></td>
											<td>
												<div class="btn-group" role="group" aria-label="Basic example">
													<button class="btn btn-info btn-sm" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" data-target="#lista_sede<?=$id_empresa?>" title="Lista de sedes" data-trigger="hover">
														<i class="fa fa-list"></i>
													</button>
													<button class="btn btn-primary btn-sm editar_empresa" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" data-target="#editar_empresa<?=$id_empresa?>" title="Editar Empresa" data-trigger="hover">
														<i class="fa fa-edit"></i>
													</button>
													<button class="btn btn-sm <?=$class?>" data-tooltip="tooltip" data-placement="bottom" title="<?=$title?>" data-trigger="hover" id="<?=$id_empresa?>">
														<?=$icon?>
													</button>
													<button class="btn btn-secondary btn-sm eliminar_empresa <?=$ver_eliminar?>" data-tooltip="tooltip" data-placement="bottom" title="Eliminar Empresa" data-trigger="hover" id="<?=$id_empresa?>">
														<i class="fa fa-trash"></i>
													</button>
												</div>
											</td>
										</tr>

										<!--Editar Empresa-->
										<div class="modal fade" id="editar_empresa<?=$id_empresa?>" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
											<div class="modal-dialog modal-lg p-2" role="document">
												<div class="modal-content">
													<form method="POST">
														<input type="hidden" value="<?=$id_empresa?>" name="id_empresa">
														<input type="hidden" value="<?=$_SESSION['id']?>" name="id_log">
														<div class="modal-header p-3">
															<h4 class="modal-title text-success font-weight-bold">Editar Empresa</h4>
														</div>
														<div class="modal-body border-0">
															<div class="row  p-3">
																<div class="col-lg-6">
																	<div class="form-group">
																		<label>Nombre</label>
																		<input type="text" class="form-control letras" name="nom_edit" maxlength="50" minlength="1" value="<?=$nombre?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label>Ciudad</label>
																		<input type="text" class="form-control letras" name="ciudad_edit" maxlength="50" minlength="1" value="<?=$ciudad?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label>Direccion</label>
																		<input type="text" class="form-control" name="dir_edit" maxlength="50" minlength="1" value="<?=$direccion?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label>Nombre Contacto</label>
																		<input type="text" class="form-control letras" name="nom_cont_edit" maxlength="50" minlength="1" value="<?=$nom_contacto?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label>Telefono</label>
																		<input type="text" class="form-control numeros" name="tel_edit" maxlength="50" minlength="1" value="<?=$telefono?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label>NIT</label>
																		<input type="text" class="form-control" name="nit_edit" maxlength="50" minlength="1" value="<?=$nit?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label>Tiempo recoleccion</label>
																		<input type="text" class="form-control numeros" name="tiempo_edit" maxlength="2" minlength="1" value="<?=$tiempo?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label>Correo</label>
																		<input type="text" class="form-control" name="email_edit" maxlength="50" minlength="1" value="<?=$email?>">
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer border-0">
															<button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
															<input type="submit" class="btn btn-success" value="Guardar Cambios">
														</div>
													</form>
												</div>
											</div>
										</div>


										<!--Lista Sede-->
										<div class="modal fade" id="lista_sede<?=$id_empresa?>" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
											<div class="modal-dialog modal-lg p-2" role="document">
												<div class="modal-content">
													<form method="POST">
														<input type="hidden" value="<?=$id_empresa?>" name="id_empresa">
														<input type="hidden" value="<?=$_SESSION['id']?>" name="id_log">
														<div class="modal-header p-3">
															<h4 class="modal-title text-success font-weight-bold">Lista de sedes (<?=$nombre?> - <?=$nit?>)</h4>
														</div>
														<div class="modal-body border-0">
															<div class="row">
																<div class="col-lg-12 p-3">
																	<ul class="list-group">
																		<?php
																		$datos_sede = $instancia_sede->mostrarSedeEmpresaIdControl($id_empresa);
																		if (count($datos_sede) > 0) {
																			foreach ($datos_sede as $sede) {
																				$id_sucursal          = $sede['id_sucursal'];
																				$nombre_sede          = $sede['nombre'];
																				$direccion_sede       = $sede['direccion'];
																				$telefono_sede        = $sede['telefono'];
																				$nombre_contacto_sede = $sede['nom_contacto'];
																				?>
																				<li class="list-group-item">
																					<?=$nombre_sede . ' - ' . $direccion_sede . ' - ' . $telefono_sede . ' - ' . $nombre_contacto_sede?>

																				</li>
																				<?php
																			}
																		} else {
																			?>
																			<li class="list-group-item text-center">No hay datos para mostrar</li>
																			<?php
																		}
																		?>
																	</ul>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
										<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . DS . 'modulos' . DS . 'empresas' . DS . 'agregarEmpresa.php';
include_once VISTA_PATH . DS . 'modulos' . DS . 'empresas' . DS . 'agregarSedes.php';
if (isset($_POST['nombre'])) {
	$instancia->guardarEmpresaControl();
}

if (isset($_POST['nom_edit'])) {
	$instancia->editarEmpresaControl();
}

include_once VISTA_PATH . 'modulos' . DS . 'configuracion' . DS . 'alerta.php';
?>
<script type="text/javascript" src="<?=PUBLIC_PATH?>js/configuracion/efectos.js"></script>
<script src="<?=PUBLIC_PATH?>js/empresas/funcionesEmpresa.js"></script>