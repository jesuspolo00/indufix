<!--Agregar empresa-->
<div class="modal fade" id="agregar_empresa" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<form method="POST">
				<input type="hidden" value="<?=$_SESSION['id']?>" name="id_log">
				<input type="hidden" value="<?=$_SESSION['super_empresa']?>" name="super_empresa">
				<div class="modal-header p-3">
					<h4 class="modal-title text-orange font-weight-bold">Agregar Empresa</h4>
				</div>
				<div class="modal-body border-0">
					<div class="row p-3">
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
								<input type="text" class="form-control letras"  name="nombre" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Ciudad <span class="text-danger">*</span></label>
								<input type="text" class="form-control letras"  name="ciudad" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Direccion <span class="text-danger">*</span></label>
								<input type="text" class="form-control"  name="direccion" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Nombre contacto <span class="text-danger">*</span></label>
								<input type="text" class="form-control letras"  name="nom_contacto" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Telefono <span class="text-danger">*</span></label>
								<input type="text" class="form-control numeros"  name="telefono" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">NIT <span class="text-danger">*</span></label>
								<input type="text" class="form-control"  name="nit" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Tiempo de recoleccion <span class="text-danger">*</span></label>
								<input type="text" class="form-control numeros" maxlength="2" minlength="1" name="tiempo" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Correo electronico <span class="text-danger">*</span></label>
								<input type="email" class="form-control"  name="email" required>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer border-0">
					<button class="btn btn-danger btn-sm" data-dismiss="modal">
						<i class="fa fa-times"></i>
						&nbsp;
						Cancelar
					</button>
					<button class="btn btn-sm btn-success" type="submit">
						<i class="fa fa-save"></i>
						&nbsp;
						Guardar
					</button>
				</div>
			</form>
		</div>
	</div>
</div>