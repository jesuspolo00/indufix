<!--Agregar vehiculo-->
<div class="modal fade" id="agregar_destino" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-md p-2" role="document">
        <div class="modal-content">
            <form method="POST">
                <input type="hidden" value="<?=$_SESSION['id']?>" name="id_log">
                <input type="hidden" value="<?=$_SESSION['super_empresa']?>" name="super_empresa">
                <div class="modal-header p-3">
                    <h4 class="modal-title text-orange font-weight-bold">Agregar Destino</h4>
                </div>
                <div class="modal-body border-0">
                    <div class="row p-3">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
                                <input type="text" class="form-control letras" maxlength="50" minlength="1" name="descripcion" required>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Nit <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" maxlength="50" minlength="1" name="nit" required>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Resolucion</label>
                                <input type="text" class="form-control" maxlength="1000" minlength="1" name="resolucion">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="font-weight-bold">Tipo de destino <span class="text-danger">*</span></label>
                                <select class="form-control" name="id_tipo" required>
                                    <option value="" selected>Seleccione una opcion...</option>
                                    <?php
                                    foreach ($datos_tipo as $key) {
                                        $id_tipo_destino = $key['id'];
                                        $nombre          = $key['nombre'];
                                        $estado          = $key['activo'];

                                        $ver = ($estado == 1) ? '' : 'd-none';
                                        ?>
                                        <option value="<?=$id_tipo_destino?>" class="<?=$ver?>"><?=$nombre?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border-0">
                   <button class="btn btn-danger btn-sm" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                    &nbsp;
                    Cancelar
                </button>
                <button class="btn btn-success btn-sm">
                    <i class="fa fa-save"></i>
                    &nbsp;
                    Guardar
                </button>
            </div>
        </form>
    </div>
</div>
