<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'residuos' . DS . 'ControlTipoResiduos.php';
$id_modulo = 1;

$instancia     = ControlTipoResiduo::singleton_tipo_residuo();
$datos_residuo = $instancia->mostrarTipoResiduoControl($id_super_empresa);
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-orange">
						<a href="<?=BASE_URL?>residuos/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-orange"></i>
						</a>
						&nbsp;
						Tipos de residuo
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
							<div class="dropdown-header">Acciones:</div>
							<?php
							$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 16, 6);
							if ($permiso) {
								?>
								<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_tipo_residuo">Agregar tipo</a>
							<?php }?>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8"></div>
						<div class="col-lg-4">
							<form>
								<div class="form-group">
									<div class="input-group mb-3">
										<input type="text" class="form-control filtro" placeholder="Buscar">
										<div class="input-group-prepend">
											<span class="input-group-text rounded-right" id="basic-addon1">
												<i class="fa fa-search"></i>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="table-responsive">
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center font-weight-bold">
										<th scope="col">#</th>
										<th scope="col">Nombre</th>
									</tr>
								</thead>
								<tbody class="buscar">
									<?php
									foreach ($datos_residuo as $datos) {
										$id_tipo = $datos['id_tipo'];
										$nombre  = $datos['nombre'];
										$estado  = $datos['estado'];

										if ($estado == 'activo') {
											$title = 'Inactivar';
											$icon  = '<i class="fa fa-times"></i>';
											$class = 'btn-danger inactivar_tipo';
										} else {
											$title = 'Activar';
											$icon  = '<i class="fa fa-check"></i>';
											$class = 'btn-success activar_tipo';
										}

										?>
										<tr class="text-center" id="tipo_residuo<?=$id_tipo;?>">
											<td><?=$id_tipo?></td>
											<td><?=$nombre?></td>
											<td>
												<div class="btn-group" role="group" aria-label="Basic example">
													<button class="btn btn-sm btn-primary editar_tipo" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" data-target="#editar_tipo<?=$id_tipo?>" title="Editar tipo de residuo" data-trigger="hover">
														<i class="fa fa-edit"></i>
													</button>
													<button class="btn btn-sm <?=$class?>" data-tooltip="tooltip" data-placement="bottom" title="<?=$title?>" data-trigger="hover" id="<?=$id_tipo?>">
														<?=$icon?>
													</button>
													<button class="btn btn-sm btn-secondary eliminar_tipo <?=$ver_eliminar?>" id="<?=$id_tipo?>" data-toggle="tooltip" data-placement="bottom" title="Eliminar tipo de residuo" data-trigger="hover">
														<i class="fa fa-trash"></i>
													</button>
												</div>
											</td>
										</tr>

										<!--Editar Tipo de residuo-->
										<div class="modal fade" id="editar_tipo<?=$id_tipo?>" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
											<div class="modal-dialog modal-md p-2" role="document">
												<div class="modal-content">
													<form method="POST">
														<input type="hidden" value="<?=$id_tipo?>" name="id_tipo">
														<div class="modal-header p-3">
															<h4 class="modal-title text-orange font-weight-bold">Editar Tipo</h4>
														</div>
														<div class="modal-body border-0">
															<div class="row p-3">
																<div class="col-lg-12">
																	<div class="form-group">
																		<label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
																		<input type="text" class="form-control letras" maxlength="50" minlength="1" name="nom_edit" value="<?=$nombre?>" required>
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer border-0">
															<button class="btn btn-danger btn-sm" data-dismiss="modal">
																<i class="fa fa-times"></i>
																&nbsp;
																Cancelar
															</button>
															<button class="btn btn-success btn-sm">
																<i class="fa fa-save"></i>
																&nbsp;
																Guardar
															</button>
														</div>
													</form>
												</div>
											</div>
										</div>
										<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'tipos_residuo' . DS . 'agregarTipo.php';
if (isset($_POST['descripcion'])) {
	$instancia->guardarTipoResiduoControl();
}

if (isset($_POST['nom_edit'])) {
	$instancia->editarTipoResiduoControl();
}
include_once VISTA_PATH . 'modulos' . DS . 'configuracion' . DS . 'alerta.php';
?>
<script src="<?=PUBLIC_PATH?>js/residuos/funcionesTipoResiduo.js"></script>
<script type="text/javascript" src="<?=PUBLIC_PATH?>js/configuracion/efectos.js"></script>