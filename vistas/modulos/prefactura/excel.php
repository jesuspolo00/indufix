<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'PhpSpreadsheet' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'prefactura' . DS . 'ControlPrefactura.php';

$instancia = ControlPrefactura::singleton_prefactura();

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

$spreadsheet = new Spreadsheet();

if (isset($_GET['fecha_desde']) && isset($_GET['fecha_hasta'])) {

    $fecha_ini = $_GET['fecha_desde'];
    $fecha_fin = $_GET['fecha_hasta'];

    $buscar = $instancia->buscarReservasPrefacturadasControl($fecha_ini, $fecha_fin);

    $spreadsheet->getProperties()
        ->setTitle('Reporte Prefacturas')
        ->setDescription('Este documento fue generado por Ecogreen');

    $sheet = $spreadsheet->setActiveSheetIndex(0);


    $estilos_cabecera = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
    ];

    $estilos_datos = [
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
    ];

    $sheet->getStyle('A1:Z1')->applyFromArray($estilos_cabecera);
    $sheet->getStyle('A:Z')->applyFromArray($estilos_datos);

    foreach (range('A', 'P') as $column) {
        $sheet->getColumnDimension($column)->setAutoSize(true);
    }

    $sheet->setCellValue('A1', 'FECHA')
        ->setCellValue('B1', 'No. RESERVA')
        ->setCellValue('C1', 'EMPRESA')
        ->setCellValue('D1', 'NIT')
        ->setCellValue('E1', 'DIRECCION')
        ->setCellValue('F1', 'CIUDAD')
        ->setCellValue('G1', 'CONTACTO')
        ->setCellValue('H1', 'TELEFONO')
        ->setCellValue('I1', 'USUARIO RESERVA')
        ->setCellValue('J1', 'USUARIO CONFIRMA')
        ->setCellValue('K1', 'USUARIO CONCILIA')
        ->setCellValue('L1', 'USUARIO PREFACTURA')
        ->setCellValue('M1', 'SUBTOTAL')
        ->setCellValue('N1', 'DESCUENTO')
        ->setCellValue('O1', 'IVA')
        ->setCellValue('P1', 'TOTAL');


    $cont = 2;
    $total_subtotal_excel = 0;
    $total_descuento_excel = 0;
    $total_iva_excel = 0;
    $total_final_excel = 0;

    foreach ($buscar as $datos) {
        $id_prefactura = $datos['id'];
        $id_reserva = $datos['id_reserva'];
        $id_empresa = $datos['id_empresa'];
        $empresa = $datos['empresa'];
        $direccion = $datos['direccion'];
        $ciudad = $datos['ciudad'];
        $telefono = $datos['telefono'];
        $contacto = $datos['contacto'];
        $nit = $datos['nit'];
        $fechareg = date('Y-m-d', strtotime($datos['fechareg']));
        $usuario_prefactura = $datos['usuario_prefactura'];
        $usuario_reserva = $datos['usuario_reserva'];
        $usuario_concilia = $datos['usuario_concilia'];
        $usuario_confirma = $datos['usuario_confirma'];
        $total_subtotal = $datos['total_subtotal'];
        $total_descuento = $datos['total_descuento'];
        $total_iva = $datos['total_iva'];
        $total_final = $datos['total_final'];

        $sheet->setCellValue('A' . $cont, $fechareg)
            ->setCellValue('B' . $cont, $id_reserva)
            ->setCellValue('C' . $cont, $empresa)
            ->setCellValue('D' . $cont, $nit)
            ->setCellValue('E' . $cont, $direccion)
            ->setCellValue('F' . $cont, $ciudad)
            ->setCellValue('G' . $cont, $contacto)
            ->setCellValue('H' . $cont, $telefono)
            ->setCellValue('I' . $cont, $usuario_reserva)
            ->setCellValue('J' . $cont, $usuario_confirma)
            ->setCellValue('K' . $cont, $usuario_concilia)
            ->setCellValue('L' . $cont, $usuario_prefactura)
            ->setCellValue('M' . $cont, '$' . number_format($total_subtotal))
            ->setCellValue('N' . $cont, '$' . number_format($total_descuento))
            ->setCellValue('O' . $cont, '$' . number_format($total_iva))
            ->setCellValue('P' . $cont, '$' . number_format($total_final));

        $cont++;

        $total_subtotal_excel += $total_subtotal;
        $total_descuento_excel += $total_descuento;
        $total_iva_excel += $total_iva;
        $total_final_excel += $total_final;
    }


    $sheet->getStyle('A' . ++$cont . ':D' . ++$cont)->applyFromArray($estilos_cabecera);
    $sheet->setCellValue('A' . $cont, 'TOTAL SUBTOTAL')
        ->setCellValue('A' . ++$cont, '$' . number_format($total_subtotal_excel));

    $sheet->setCellValue('B' . --$cont, 'TOTAL DESCUENTO')
        ->setCellValue('B' . ++$cont, '$' . number_format($total_descuento_excel));

    $sheet->setCellValue('C' . --$cont, 'TOTAL IVA')
        ->setCellValue('C' . ++$cont, '$' . number_format($total_iva_excel));

    $sheet->setCellValue('D' . --$cont, 'TOTAL FINAL')
        ->setCellValue('D' . ++$cont, '$' . number_format($total_final_excel));


    $spreadsheet->getActiveSheet()->setTitle('Hoja 1');
    $spreadsheet->setActiveSheetIndex(0);
    $nombreDelDocumento = "Reporte_Prefactura.xlsx";

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $nombreDelDocumento . '"');
    header('Cache-Control: max-age=0');

    $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer->save('php://output');
}
