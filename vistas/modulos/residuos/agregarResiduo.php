<!--Agregar vehiculo-->
<div class="modal fade" id="agregar_residuo" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog modal-md p-2" role="document">
		<div class="modal-content">
			<form method="POST">
				<input type="hidden" value="<?=$_SESSION['id']?>" name="id_log">
				<input type="hidden" value="<?=$_SESSION['super_empresa']?>" name="super_empresa">
				<div class="modal-header p-3">
					<h4 class="modal-title text-orange font-weight-bold">Agregar Residuo</h4>
				</div>
				<div class="modal-body border-0">
					<div class="row p-3">
						<div class="col-lg-12">
							<div class="form-group">
								<label class="font-weight-bold">Descripcion <span class="text-danger">*</span></label>
								<input type="text" class="form-control letras" maxlength="50" minlength="1" name="descripcion" required>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label class="font-weight-bold">Codigo (opcional)</label>
								<input type="text" class="form-control" maxlength="5" minlength="1" name="codigo">
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label class="font-weight-bold">Valor Unitario <span class="text-danger">*</span></label>
								<input type="text" class="form-control numeros" maxlength="10" minlength="1" name="valor" required>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label class="font-weight-bold">Tipo de residuo <span class="text-danger">*</span></label>
								<select class="form-control" name="id_tipo" required>
									<option value="" selected>Seleccione una opcion...</option>
									<?php
									foreach ($datos_tipo as $key) {
										$id_tipo_residuo = $key['id_tipo'];
										$nombre          = $key['nombre'];
										$estado          = $key['estado'];

										if ($estado == 'activo') {
											?>
											<option value="<?=$id_tipo_residuo?>"><?=$nombre?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer border-0">
					<button class="btn btn-danger btn-sm" data-dismiss="modal">
						<i class="fa fa-times"></i>
						&nbsp;
						Cancelar
					</button>
					<button class="btn btn-success btn-sm">
						<i class="fa fa-save"></i>
						&nbsp;
						Guardar
					</button>
				</div>
			</form>
		</div>
	</div>
</div>