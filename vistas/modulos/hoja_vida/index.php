<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'hoja_vida' . DS . 'ControlHojaVida.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlVehiculo.php';
require_once CONTROL_PATH . 'usuario' . DS . 'ControlUsuario.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlTipoVehiculo.php';

$instancia          = ControlHojaVida::singleton_hoja_vida();
$instancia_vehiculo = ControlVehiculo::singleton_vehiculo();
$instancia_tipo     = ControlTipoVehiculo::singleton_tipo_vehiculo();
$instancia_usuario  = ControlUsuario::singleton_usuario();

if (isset($_GET['vehiculo'])) {

	$id_vehiculo = base64_decode($_GET['vehiculo']);

	$datos_vehiculo  = $instancia_vehiculo->mostrarDatosVehiculosIdControl($id_vehiculo);
	$descripcion     = $datos_vehiculo['descripcion'];
	$placa           = $datos_vehiculo['placa'];
	$marca           = $datos_vehiculo['marca'];
	$modelo          = $datos_vehiculo['modelo'];
	$estado_vehiculo = $datos_vehiculo['estado'];
	$id_user         = $datos_vehiculo['id_user'];
	$fechareg        = $datos_vehiculo['fechareg'];

	$datos_componente = $instancia->mostrarComponenteVehiculoControl($id_vehiculo);

	$datos_usuario     = $instancia_usuario->mostrarDatosUsuariosIdControl($id_user);
	$nombre_usuario    = $datos_usuario['nombre'] . ' ' . $datos_usuario['apellido'];
	$documento_usuario = $datos_usuario['documento'];
	$estado_usuario    = $datos_usuario['estado'];

	$datos_hoja      = $instancia->mostrarHojaVidaVehiculoControl($id_vehiculo);
	$mantenimiento   = $datos_hoja['mantenimiento'];
	$fecha_adquirido = $datos_hoja['fecha_adquisicion'];
	$fecha_vence     = $datos_hoja['fecha_vence'];
	$cont_garantia   = $datos_hoja['cont_garantia'];
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow mb-4">
					<!-- Card Header - Dropdown -->
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-orange">
							<a href="<?=BASE_URL?>vehiculos/index" class="text-decoration-none">
								<i class="fa fa-arrow-left text-orange"></i>
							</a>
							&nbsp;
							Hoja de vida (<?=$descripcion . ' ' . $placa?>)
						</h4>
					</div>
					<div class="card-body">
						<form method="POST">
							<input type="hidden" name="id_vehiculo" value="<?=$id_vehiculo?>">
							<input type="hidden" name="id_log" value="<?=$_SESSION['id']?>">
							<div class="row">
								<div class="col-lg-4">
									<div class="form-group">
										<label>Nombre</label>
										<div class="input-group input-group-sm mb-3">
											<input type="text" class="form-control letras" name="descripcion" required maxlength="20" minlength="2" value="<?=$descripcion?>">
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Placa</label>
										<div class="input-group input-group-sm mb-3">
											<input type="text" class="form-control letras" name="placa" required maxlength="20" minlength="2" value="<?=$placa?>">
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Marca</label>
										<div class="input-group input-group-sm mb-3">
											<input type="text" class="form-control letras" name="marca" required maxlength="20" minlength="2" value="<?=$marca?>">
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Modelo</label>
										<div class="input-group input-group-sm mb-3">
											<input type="text" class="form-control letras" name="modelo" maxlength="20" minlength="2" value="<?=$modelo?>">
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Frecuencia de mantenimiento (a&ntilde;o)</label>
										<div class="input-group input-group-sm mb-3">
											<input type="text" class="form-control numeros" maxlength="2" minlength="1" name="mantenimiento" value="<?=$mantenimiento?>">
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Fecha de adquisicion</label>
										<div class="input-group input-group-sm mb-3">
											<input type="date" class="form-control" name="adquirido" value="<?=$fecha_adquirido?>">
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Fecha de garantia</label>
										<div class="input-group input-group-sm mb-3">
											<input type="date" class="form-control" name="garantia" value="<?=$fecha_vence?>">
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Contacto de garantia</label>
										<div class="input-group input-group-sm mb-3">
											<input type="text" class="form-control letras" maxlength="20" minlength="1" name="cont_garantia" value="<?=$cont_garantia?>">
										</div>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="table-responsive">
										<table class="table table-hover border" width="100%" cellspacing="0">
											<thead>
												<tr class="text-center font-weight-bold">
													<th colspan="6">
														Componentes del Vehiculo
													</th>
													<?php
													if (count($datos_componente) <= 0) {
														?>
														<th>
															<div class="dropdown no-arrow">
																<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
																</a>
																<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
																	<div class="dropdown-header">Acciones:</div>
																	<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_componente">Agregar Componente</a>
																</div>
															</div>
														</th>
														<?php
													}
													?>
												</tr>
												<tr class="text-center font-weight-bold">
													<th>Descripcion</th>
													<th>Fabricante</th>
													<th>Licencia</th>
													<th>Fecha Registro</th>
													<th>Estado</th>
												</tr>
											</thead>
											<tbody class="buscar">
												<?php
												foreach ($datos_componente as $componente) {
													$id_componente          = $componente['id'];
													$descripcion_componente = $componente['descripcion'];
													$fabricante             = $componente['fabricante'];
													$licencia               = $componente['licencia'];
													$fecha                  = $componente['fecha'];

													$anio        = date("Y", strtotime($fecha));
													$anio_actual = date('Y');

													$mes_dia        = date('m-d', strtotime($fecha));
													$mes_dia_actual = date('m-d');

													if ($mes_dia <= $mes_dia_actual && $anio <= $anio_actual) {
														$estado = 'Expirada';
													} else {
														$estado = 'Vigente';
													}

													?>
													<tr class="text-center">
														<td><?=$descripcion_componente?></td>
														<td><?=$fabricante?></td>
														<td><?=$licencia?></td>
														<td><?=$fecha?></td>
														<td><?=$estado?></td>
													</tr>
													<?php
												}
												?>
											</tbody>
										</table>
									</div>
								</div>
								<div class="col-lg-12 mt-3">
									<div class="table-responsive">
										<table class="table table-hover border" width="100%" cellspacing="0">
											<thead>
												<tr class="text-center font-weight-bold">
													<th colspan="4">
														Ubicacion y asignacion del vehiculo
													</th>
												</tr>
												<tr class="text-center font-weight-bold">
													<th>Dependencia</th>
													<th>Usuario o responsable</th>
													<th>Fecha de asignacion</th>
													<th>Estado Usuario</th>
												</tr>
											</thead>
											<tbody class="buscar">
												<tr class="text-center">
													<td>Ecogreen</td>
													<td><?=$nombre_usuario?></td>
													<td><?=$fechareg?></td>
													<td><?=$estado_usuario?></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="col-lg-12 mt-4">
									<div class="form-group float-left">
										<a href="#" class="btn btn-secondary btn-sm">
											<i class="fa fa-print"></i>
											Imprimir
										</a>
									</div>
									<div class="form-group float-right">
										<button class="btn btn-success btn-sm">
											<i class="fa fa-save"></i>
											&nbsp;
											Guardar
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once VISTA_PATH . 'script_and_final.php';
	include_once VISTA_PATH . 'modulos' . DS . 'hoja_vida' . DS . 'agregarComponente.php';

	if (isset($_POST['descripcion'])) {
		$instancia->actualizarHojaVidaControl();
	}

	if (isset($_POST['descripcion_componente'])) {
		$instancia->registrarComponenteHojaVidaControl();
	}
}
?>