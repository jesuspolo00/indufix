<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'programacion' . DS . 'ControlProgramacion.php';
$instancia    = ControlProgramacion::singleton_programacion();
$certificados = $instancia->certificadosSolicitadosControl();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-orange">
                        Certificados
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-4 ml-auto">
                            <form>
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control filtro" placeholder="Buscar">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text rounded-right" id="basic-addon1">
                                                <i class="fa fa-search"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover border table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th></th>
                                    <th scope="col">No.Reserva</th>
                                    <th scope="col">Empresa</th>
                                    <th scope="col">Ciudad</th>
                                    <th scope="col">Direccion</th>
                                    <th scope="col">Vehiculo</th>
                                    <th scope="col">Conductor</th>
                                </tr>
                            </thead>
                            <tbody class="buscar">
                                <?php
                                foreach ($certificados as $datos) {
                                    $id_solicitud     = $datos['id'];
                                    $id_empresa       = $datos['id_empresa'];
                                    $id_sucursal      = $datos['id_sucursal'];
                                    $id_reserva       = $datos['id_reserva'];
                                    $id_user_solicita = $datos['id_user_solicita'];
                                    $estado           = $datos['estado'];
                                    $empresa          = $datos['empresa'];
                                    $direccion        = $datos['direccion'];
                                    $ciudad           = $datos['ciudad'];
                                    $vehiculo         = $datos['vehiculo'];
                                    $usuario          = $datos['usuario'];

                                    $sede = ($id_sucursal != 0) ? '<span class="badge badge-info">Sede</span>' :
                                    '<span class="badge badge-success">Empresa</span>';

                                    $boton_visible = 'd-none';
                                    $span          = '';

                                    if ($estado == 1) {
                                        $boton_visible = '';
                                        $span          = 'd-none';
                                    } else {
                                        $confirmado = '<span class="badge badge-success p-1">Autorizado</span>';
                                    }
                                    ?>
                                    <tr class="text-center">
                                        <td><?=$sede?></td>
                                        <td><?=$id_reserva?></td>
                                        <td><?=$empresa?></td>
                                        <td><?=$ciudad?></td>
                                        <td><?=$direccion?></td>
                                        <td><?=$vehiculo?></td>
                                        <td><?=$usuario?></td>
                                        <td class="<?=$boton_visible?>">
                                            <form method="POST">
                                                <input type="hidden" name="id_solicitud" value="<?=$id_solicitud?>">
                                                <input type="hidden" name="id_confirma" value="<?=$id_log?>">
                                                <button type="submit" class="btn btn-success btn-sm">
                                                    <i class="fa fa-check"></i>
                                                    &nbsp;
                                                    Autorizar
                                                </button>
                                            </form>
                                        </td>
                                        <td class="<?=$span?>">
                                            <?=$confirmado?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
if (isset($_POST['id_confirma'])) {
    $instancia->confirmarCertificadoControl();
}
include_once VISTA_PATH . 'script_and_final.php';
