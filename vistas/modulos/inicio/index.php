<?php
if ($id_empresa_log != 0 && $_SESSION['rol'] == 8) {
	$datos_reservas = $instancia_apartar->mostrarReservasEmpresaIdControl($id_empresa_log);
	?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card shadow mb-4">
					<!-- Card Header - Dropdown -->
					<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
						<h4 class="m-0 font-weight-bold text-success">
							Certificados
						</h4>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-4 ml-auto">
								<form>
									<div class="form-group">
										<div class="input-group mb-3">
											<input type="text" class="form-control filtro" placeholder="Buscar">
											<div class="input-group-prepend">
												<span class="input-group-text rounded-right" id="basic-addon1">
													<i class="fa fa-search"></i>
												</span>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table table-hover table-borderless table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center font-weight-bold">
										<th></th>
										<th scope="col">No.Reserva</th>
										<th scope="col">Empresa</th>
										<th scope="col">Ciudad</th>
										<th scope="col">Direccion</th>
										<th scope="col">Vehiculo</th>
										<th scope="col">Conductor</th>
									</tr>
								</thead>
								<tbody class="buscar">
									<?php
									foreach ($datos_reservas as $reserva) {
										$id_reserva   = $reserva['id_reserva'];
										$id_empresa   = $reserva['id_empresa'];
										$id_sucursal  = $reserva['id_sucursal'];
										$empresa      = $reserva['empresa'];
										$direccion    = $reserva['direccion'];
										$ciudad       = $reserva['ciudad'];
										$vehiculo     = $reserva['vehiculo'];
										$conductor    = $reserva['conductor'];
										$prefacturado = $reserva['prefacturado'];
										$recepcion    = $reserva['recepcion'];
										$certificado  = $reserva['certificado'];
										$fecha        = $reserva['fecha_apartado'];

										$sede = ($id_sucursal != 0) ? '<span class="badge badge-info">Sede</span>' :
										'<span class="badge badge-success">Empresa</span>';

										$ver_certificado = 'd-none';
										$ver_span        = 'd-none';
										$ver_boton       = 'd-none';

										if ($certificado == 1 && $prefacturado != '') {
											$span            = '<span class="badge badge-warning p-1">En espera de autorizacion</span>';
											$ver_span        = '';
											$ver_boton       = 'd-none';
											$ver_certificado = 'd-none';
										} else if ($certificado == 2 && $prefacturado != '') {
											$url_certificado = BASE_URL . 'imprimir/imprimirCertificado?reserva=' .
											base64_encode($id_reserva) . '&fecha=' . base64_encode($fecha);
											$ver_certificado = '';
											$ver_span        = 'd-none';
											$ver_boton       = 'd-none';
										} else if ($certificado == '' && $prefacturado != '') {
											$ver_boton       = '';
											$ver_certificado = 'd-none';
											$ver_span        = 'd-none';
										}

										?>
										<tr class="text-center">
											<td><?=$sede?></td>
											<td><?=$id_reserva?></td>
											<td><?=$empresa?></td>
											<td><?=$ciudad?></td>
											<td><?=$direccion?></td>
											<td><?=$vehiculo?></td>
											<td><?=$conductor?></td>
											<td class="<?=$ver_boton?>">
												<form method="POST">
													<input type="hidden" name="id_empresa" value="<?=$id_empresa?>">
													<input type="hidden" name="id_sucursal" value="<?=$id_sucursal?>">
													<input type="hidden" name="id_user" value="<?=$id_log?>">
													<input type="hidden" name="id_reserva" value="<?=$id_reserva?>">
													<button type="submit" class="btn btn-success btn-sm" id="<?=$id_reserva?>">
														<i class="fas fa-clipboard-check"></i>
														&nbsp;
														Solicitar Certificado
													</button>
												</form>
											</td>
											<td class="<?=$ver_span?>"><?=$span?></td>
											<td class="<?=$ver_certificado?>">
												<a href="<?=$url_certificado?>" class="btn btn-warning btn-sm" target="_blank" data-toggle="tooltip" data-placement="bottom" title="Descargar certificado" data-trigger="hover">
													<i class="fas fa-file-pdf"></i>
												</a>
											</td>
										</tr>
										<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}
