<!--Agregar vehiculo-->
<div class="modal fade" id="agregar_vehiculo" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog modal-lg p-2" role="document">
		<div class="modal-content">
			<form method="POST">
				<input type="hidden" value="<?=$_SESSION['super_empresa']?>" name="super_empresa">
				<input type="hidden" value="<?=$_SESSION['id']?>" name="id_log">
				<div class="modal-header p-3">
					<h4 class="modal-title text-orange font-weight-bold">Agregar Vehiculo</h4>
				</div>
				<div class="modal-body border-0">
					<div class="row p-3">
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Descripcion <span class="text-danger">*</span></label>
								<input type="text" class="form-control letras" maxlength="50" minlength="1" name="descripcion" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Placa <span class="text-danger">*</span></label>
								<input type="text" class="form-control" maxlength="50" minlength="1" name="placa" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Marca</label>
								<input type="text" class="form-control" maxlength="50" minlength="1" name="marca">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Modelo</label>
								<input type="text" class="form-control" maxlength="50" minlength="1" name="modelo">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Tipo de vehiculo <span class="text-danger">*</span></label>
								<select class="form-control" name="id_tipo" required>
									<option selected value="">Seleccione una opcion...</option>
									<?php
									foreach ($datos_tipo as $datos) {
										$id_tipo = $datos['id_tipo'];
										$nombre  = $datos['nombre'];
										$estado  = $datos['estado'];

										if ($estado != 'inactivo') {
											?>
											<option value="<?=$id_tipo?>"><?=$nombre?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Usuario a cargo <span class="text-danger">*</span></label>
								<select class="form-control" name="id_user" required>
									<option selected value="">Seleccione una opcion...</option>
									<?php
									foreach ($datos_user as $datos_usuario) {
										$id_user  = $datos_usuario['id_user'];
										$nombre   = $datos_usuario['nombre'];
										$apellido = $datos_usuario['apellido'];
										$estado   = $datos_usuario['estado'];

										if ($estado != 'inactivo') {
											?>
											<option value="<?=$id_user?>"><?=$nombre . ' ' . $apellido?></option>
											<?php
										}
									}
									?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer border-0">
					<button class="btn btn-danger btn-sm" data-dismiss="modal">
						<i class="fa fa-times"></i>
						&nbsp;
						Cancelar
					</button>
					<button class="btn btn-success btn-sm">
						<i class="fa fa-save"></i>
						&nbsp;
						Registrar
					</button>
				</div>
			</form>
		</div>
	</div>
</div>