<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}

include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
$id_modulo = 1;

$instancia    = ControlPerfil::singleton_perfil();
$datos_perfil = $instancia->mostrarPerfilesControl($id_super_empresa);
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-orange">
						<a href="<?=BASE_URL?>configuracion/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-orange"></i>
						</a>
						&nbsp;
						Perfiles
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
							<div class="dropdown-header">Acciones:</div>
							<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_perfil">Agregar Perfil</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8"></div>
						<div class="col-lg-4">
							<form>
								<div class="form-group">
									<div class="input-group mb-3">
										<input type="text" class="form-control filtro" placeholder="Buscar">
										<div class="input-group-prepend">
											<span class="input-group-text rounded-right" id="basic-addon1">
												<i class="fa fa-search"></i>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="table-responsive">
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center font-weight-bold">
										<th scope="col">#</th>
										<th scope="col">Descripcion</th>
									</tr>
								</thead>
								<tbody class="buscar">
									<?php
									foreach ($datos_perfil as $datos) {
										$id_perfil = $datos['id_perfil'];
										$nombre    = $datos['nombre'];
										$estado    = $datos['estado'];

										$ver_perfil = ($id_perfil == 1) ? 'd-none' : '';

										?>
										<tr class="text-center <?=$ver_perfil?>" id="perfil<?=$id_perfil;?>">
											<td><?=$id_perfil?></td>
											<td><?=$nombre?></td>
											<td>
												<div class="btn-group" role="group" aria-label="Basic example">
													<button class="btn btn-sm btn-primary editar_perfil" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" data-target="#editar_perfil<?=$id_perfil?>" title="Editar Perfil" data-trigger="hover">
														<i class="fa fa-edit"></i>
													</button>
													<button class="btn btn-sm btn-secondary eliminar_perfil <?=$ver_eliminar?>" id="<?=$id_perfil?>" data-toggle="tooltip" data-placement="bottom" title="Eliminar Perfil" data-trigger="hover">
														<i class="fa fa-trash"></i>
													</button>
												</div>
											</td>
										</tr>


										<!--Editar Perfiles-->
										<div class="modal fade" id="editar_perfil<?=$id_perfil?>" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
											<div class="modal-dialog modal-md p-2" role="document">
												<div class="modal-content">
													<form method="POST">
														<input type="hidden" value="<?=$id_perfil?>" name="id_perfil">
														<div class="modal-header p-3">
															<h4 class="modal-title text-success font-weight-bold">Editar Perfil</h4>
														</div>
														<div class="modal-body border-0">
															<div class="row p-3">
																<div class="col-lg-12">
																	<div class="form-group">
																		<label class="font-weight-bold">Descripcion <span class="text-danger">*</span></label>
																		<input type="text" class="form-control letras" maxlength="50" minlength="1" name="nom_edit" value="<?=$nombre?>" required>
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer border-0">
															<button class="btn btn-danger btn-sm" data-dismiss="modal">
																<i class="fa fa-times"></i>
																&nbsp;
																Cancelar
															</button>
															<button type="submit" class="btn btn-success btn-sm">
																<i class="fa fa-save"></i>
																&nbsp;
																Guardar
															</button>
														</div>
													</form>
												</div>
											</div>
										</div>
										<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . DS . 'modulos' . DS . 'perfiles' . DS . 'agregarPerfil.php';
if (isset($_POST['descripcion'])) {
	$instancia->guardarPerfilControl();
}

if (isset($_POST['nom_edit'])) {
	$instancia->editarPerfilesControl();
}
include_once VISTA_PATH . 'modulos' . DS . 'configuracion' . DS . 'alerta.php';
?>
<script type="text/javascript" src="<?=PUBLIC_PATH?>js/configuracion/efectos.js"></script>
<script src="<?=PUBLIC_PATH?>js/perfiles/funcionesPerfil.js"></script>