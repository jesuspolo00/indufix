<?php

require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlEmpresa.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlSede.php';
$id_modulo = 1;

$instancia      = ControlEmpresa::singleton_empresa();
$instancia_sede = ControlSede::singleton_sede();
$datos_empresa  = $instancia->mostrarEmpresasControl($id_super_empresa);
$datos_sede     = $instancia_sede->mostrarSedecontrol($id_super_empresa);
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-orange">
						<a href="<?=BASE_URL?>configuracion/index" class="text-decoration-none">
							<i class="fa fa-arrow-left text-orange"></i>
						</a>
						&nbsp;
						Sedes
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
							<div class="dropdown-header">Acciones:</div>
							<?php
							if (count($datos_empresa) > 0) {
								$permiso = $instancia_permisos->consultarPermisoControl($id_log, $id_modulo, 4, 6);
								if ($permiso) {
									?>
									<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_sede">Agregar Sede</a>
									<?php
								}
							}
							?>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8"></div>
						<div class="col-lg-4">
							<form>
								<div class="form-group">
									<div class="input-group mb-3">
										<input type="text" class="form-control filtro" placeholder="Buscar">
										<div class="input-group-prepend">
											<span class="input-group-text rounded-right" id="basic-addon1">
												<i class="fa fa-search"></i>
											</span>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="table-responsive">
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center font-weight-bold">
										<th scope="col">Sede</th>
										<th scope="col">Empresa</th>
										<th scope="col">Ciudad</th>
										<th scope="col">Direccion</th>
										<th scope="col">Nombre contacto</th>
										<th scope="col">Telefono</th>
										<th scope="col">NIT</th>
										<th scope="col">Correo</th>
										<th scope="col">Tiempo(dias)</th>
									</tr>
								</thead>
								<tbody class="buscar">
									<?php
									foreach ($datos_sede as $datos) {
										$id_sucursal  = $datos['id_sucursal'];
										$id_empresa   = $datos['id_empresa'];
										$nombre       = $datos['nombre'];
										$ciudad       = $datos['ciudad'];
										$direccion    = $datos['direccion'];
										$nom_contacto = $datos['nom_contacto'];
										$nit          = $datos['nit'];
										$telefono     = $datos['telefono'];
										$tiempo       = $datos['tiempo'];
										$email        = $datos['email'];
										$estado       = $datos['estado'];

										$consultar_empresa = $instancia_sede->mostrarempresaControl($id_empresa);
										$nombre_empresa    = $consultar_empresa['nombre'];

										if ($estado == 'activo') {
											$title = 'Inactivar';
											$icon  = '<i class="fa fa-times"></i>';
											$class = 'btn-danger inactivar_sede';
										} else {
											$title = 'Activar';
											$icon  = '<i class="fa fa-check"></i>';
											$class = 'btn-success activar_sede';
										}
										?>

										<tr class="text-center" id="sede<?=$id_sucursal;?>">
											<td><?=$nombre?></td>
											<td><?=$nombre_empresa?></td>
											<td><?=$ciudad?></td>
											<td><?=$direccion?></td>
											<td><?=$nom_contacto?></td>
											<td><?=$telefono?></td>
											<td><?=$nit?></td>
											<td><?=$email?></td>
											<td><?=$tiempo?></td>
											<td>
												<div class="btn-group" role="group" aria-label="Basic example">
													<button class="btn btn-primary btn-sm editar_sede" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" data-target="#editar_sede<?=$id_sucursal?>" title="Editar Sede" data-trigger="hover">
														<i class="fa fa-edit"></i>
													</button>
													<button class="btn btn-sm <?=$class?>" data-tooltip="tooltip" data-placement="bottom" title="<?=$title?>" data-trigger="hover" id="<?=$id_sucursal?>">
														<?=$icon?>
													</button>
													<button class="btn btn-secondary btn-sm eliminar_sede <?=$ver_eliminar?>" data-tooltip="tooltip" data-placement="bottom" title="Eliminar Sede" data-trigger="hover" id="<?=$id_sucursal?>">
														<i class="fa fa-trash"></i>
													</button>
												</div>
											</td>
										</tr>


										<!--Editar Empresa-->
										<div class="modal fade" id="editar_sede<?=$id_sucursal?>" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">

											<div class="modal-dialog modal-lg p-2" role="document">
												<div class="modal-content">
													<form method="POST">
														<input type="hidden" value="<?=$id_sucursal?>" name="id_sede">
														<input type="hidden" value="<?=$_SESSION['id']?>" name="id_log">
														<div class="modal-header p-3">
															<h4 class="modal-title text-orange font-weight-bold">Editar Sede</h4>
														</div>
														<div class="modal-body border-0">
															<div class="row  p-3">
																<div class="col-lg-6">
																	<div class="form-group">
																		<label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
																		<input type="text" required class="form-control letras" name="nom_edit" maxlength="50" minlength="1" value="<?=$nombre?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label class="font-weight-bold">Ciudad <span class="text-danger">*</span></label>
																		<input type="text" required class="form-control letras" name="ciudad_edit" maxlength="50" minlength="1" value="<?=$ciudad?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label class="font-weight-bold">Direccion <span class="text-danger">*</span></label>
																		<input type="text" required class="form-control" name="dir_edit" maxlength="50" minlength="1" value="<?=$direccion?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label class="font-weight-bold">Nombre Contacto <span class="text-danger">*</span></label>
																		<input type="text" required class="form-control letras" name="nom_cont_edit" maxlength="50" minlength="1" value="<?=$nom_contacto?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label class="font-weight-bold">Telefono</label>
																		<input type="text" class="form-control numeros" name="tel_edit" maxlength="20" minlength="1" value="<?=$telefono?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label class="font-weight-bold">NIT <span class="text-danger">*</span></label>
																		<input type="text" required class="form-control" name="nit_edit" maxlength="30" minlength="1" value="<?=$nit?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label class="font-weight-bold">Tiempo recoleccion <span class="text-danger">*</span></label>
																		<input type="text" required class="form-control numeros" name="tiempo_edit" maxlength="2" minlength="1" value="<?=$tiempo?>">
																	</div>
																</div>
																<div class="col-lg-6">
																	<div class="form-group">
																		<label class="font-weight-bold">Correo <span class="text-danger">*</span></label>
																		<input type="text" required class="form-control" name="email_edit" maxlength="50" minlength="1" value="<?=$email?>">
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer border-0">
															<button class="btn btn-danger btn-sm" data-dismiss="modal">
																<i class="fa fa-times"></i>
																&nbsp;
																Cancelar
															</button>
															<button class="btn btn-success btn-sm">
																<i class="fa fa-save"></i>
																&nbsp;
																Guardar
															</button>
														</div>
													</form>
												</div>
											</div>
										</div>
										<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . DS . 'modulos' . DS . 'sedes' . DS . 'agregarSedes.php';

if (isset($_POST['nombre_sede'])) {
	$instancia_sede->agregarSedeControl();
}

if (isset($_POST['nom_edit'])) {
	$instancia_sede->editarSedeControl();
}

include_once VISTA_PATH . 'modulos' . DS . 'configuracion' . DS . 'alerta.php';
?>
<script type="text/javascript" src="<?=PUBLIC_PATH?>js/configuracion/efectos.js"></script>
<script src="<?=PUBLIC_PATH?>js/sedes/funcionesSedes.js"></script>