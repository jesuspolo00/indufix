<!--Agregar empresa-->
<div class="modal fade" id="agregar_sede" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog modal-lg p-2" role="document">
		<div class="modal-content">
			<form method="POST">
				<input type="hidden" value="<?=$_SESSION['id']?>" name="id_log">
				<div class="modal-header p-3">
					<h4 class="modal-title text-orange font-weight-bold">Agregar Sede</h4>
				</div>
				<div class="modal-body border-0">
					<div class="row p-3">
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Empresa <span class="text-danger">*</span></label>
								<select name="id_empresa" class="form-control" id="id_empresa" required>
									<option value="" selected>Seleccione una opcion...</option>
									<?php
									foreach ($datos_empresa as $datos) {
										$id_empresa = $datos['id_empresa'];
										$nombre     = $datos['nombre'];
										$nit        = $datos['nit'];
										?>
										<option value="<?=$id_empresa?>"><?=$nombre?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
								<input type="text" class="form-control letras" maxlength="50" minlength="1" name="nombre_sede" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Ciudad <span class="text-danger">*</span></label>
								<input type="text" class="form-control letras" maxlength="50" minlength="1" name="ciudad_sede" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Direccion <span class="text-danger">*</span></label>
								<input type="text" class="form-control" maxlength="50" minlength="1" name="direccion_sede" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Nombre contacto <span class="text-danger">*</span></label>
								<input type="text" class="form-control letras" maxlength="50" minlength="1" name="nom_contacto_sede" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Telefono</label>
								<input type="text" class="form-control numeros" maxlength="50" minlength="1" name="telefono_sede">
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Nit <span class="text-danger">*</span></label>
								<input type="text" class="form-control" maxlength="30" minlength="1" name="nit" id="nitE" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Tiempo de recoleccion <span class="text-danger">*</span></label>
								<input type="text" class="form-control numeros" maxlength="2" minlength="1" name="tiempo_sede" required>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label class="font-weight-bold">Correo electronico <span class="text-danger">*</span></label>
								<input type="text" class="form-control" maxlength="50" minlength="1" name="email_sede" required>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer border-0">
					<button class="btn btn-danger btn-sm" data-dismiss="modal">
						<i class="fa fa-times"></i>
						&nbsp;
						Cancelar
					</button>
					<button class="btn btn-success btn-sm">
						<i class="fa fa-save"></i>
						&nbsp;
						Guardar
					</button>
				</div>
			</form>
		</div>
	</div>
</div>