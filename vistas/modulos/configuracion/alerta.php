<!--Alerta-->
<div class="modal fade" id="alerta" tabindex="-1" role="dialog" aria-hidden="true" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog modal-md modal-dialog-centered p-3" role="document">
		<div class="modal-content">
			<div class="modal-body border-0 text-center">
				<i class="fas fa-exclamation-triangle fa-2x text-warning"></i>
				<h4 class="text-warning">Adventercia</h4>
				<p class="mt-3">
					Le recomendamos realizar las configuraciones desde una computadora para que no ocurra ningun inconveniente.
				</p>
				<a href="<?=BASE_URL?>inicio" class="btn btn-warning btn-sm mx-auto">
					Aceptar
				</a>
			</div>
		</div>
	</div>
</div>