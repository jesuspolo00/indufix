<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'apartar' . DS . 'ControlApartar.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlEmpresa.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlVehiculo.php';
require_once CONTROL_PATH . 'programacion' . DS . 'ControlProgramacion.php';
require_once CONTROL_PATH . 'usuario' . DS . 'ControlUsuario.php';
require_once CONTROL_PATH . 'recepcion' . DS . 'ControlRecepcion.php';

$instancia = ControlProgramacion::singleton_programacion();
$instancia_usuario = ControlUsuario::singleton_usuario();
$instancia_apartar = ControlApartar::singleton_apartar();
$instancia_empresa = ControlEmpresa::singleton_empresa();
$instancia_vehiculo = ControlVehiculo::singleton_vehiculo();
$instancia_recepcion = ControlRecepcion::singleton_recepcion();
include_once VISTA_PATH . 'modulos' . DS . 'inicio' . DS . 'index.php';
if(isset($_POST['id_reserva'])){
	$instancia->solicitarCertificadoControl();
}
include_once VISTA_PATH . 'script_and_final.php';