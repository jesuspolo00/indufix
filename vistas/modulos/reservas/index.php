<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'programacion' . DS . 'ControlProgramacion.php';
require_once CONTROL_PATH . 'recepcion' . DS . 'ControlRecepcion.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlEmpresa.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlSede.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlVehiculo.php';

$instancia           = ControlProgramacion::singleton_programacion();
$instancia_recepcion = ControlRecepcion::singleton_recepcion();
$instancia_empresa   = ControlEmpresa::singleton_empresa();
$instancia_sede      = ControlSede::singleton_sede();
$instancia_vehiculo  = ControlVehiculo::singleton_vehiculo();

$datos_vehiculo = $instancia_vehiculo->mostrarDatosVehiculosControl($id_super_empresa);
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow mb-4">
				<!-- Card Header - Dropdown -->
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-orange">
						Reporte de reservas
					</h4>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-12 mb-2">
							<form method="POST">
								<div class="row">
									<div class="col-lg-5">
										<input type="date" class="form-control filtro_change" name="fecha_ini" data-toggle="tooltip" data-placement="top" title="Fecha desde" data-trigger="hover">
									</div>
									<div class="col-lg-5">
										<div class="form-group">
											<input type="date" class="form-control filtro_change" name="fecha_fin" data-toggle="tooltip" data-placement="top" title="Fecha hasta" data-trigger="hover" value="<?=date('Y-m-d')?>">
										</div>
									</div>
									<div class="col-lg-2 text-center">
										<div class="form-group mt-1">
											<button type="submit" class="btn btn-sm btn-primary">
												<i class="fa fa-search"></i>
												&nbsp;
												Buscar
											</button>
											<?php if (isset($_POST['fecha_ini'])) {?>
												<a href="<?=BASE_URL?>reservas/reporte?fecha_desde=<?=$_POST['fecha_ini']?>&fecha_hasta=<?=$_POST['fecha_fin']?>" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="right" title="Descargar Reporte" data-trigger="hover">
													<i class="fa fa-file-excel"></i>
													&nbsp;
													Descargar
												</a>
											<?php }?>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="table-responsive col-lg-12">
							<table class="table table-hover border table-sm" width="100%" cellspacing="0">
								<thead>
									<tr class="text-center font-weight-bold">
										<th scope="col">#</th>
										<th scope="col">Fecha</th>
										<th scope="col">Empresa</th>
										<th scope="col">Placa del vehiculo</th>
										<th scope="col">No. Movilizacion</th>
										<th scope="col">No. Recepcion</th>
										<th scope="col">Certificado</th>
										<th scope="col">Ciudad</th>
										<th scope="col">Fecha de recoleccion</th>
										<th scope="col">Nombre del contacto</th>
										<th scope="col">Recepcion</th>
									</tr>
								</thead>
								<?php
								if (isset($_POST['fecha_ini'])) {
									$fecha_ini = $_POST['fecha_ini'];
									$fecha_fin = $_POST['fecha_fin'];
									$buscar    = $instancia->buscarReservaFechaControl($fecha_ini, $fecha_fin);
									?>
									<tbody class="buscar">
										<?php
										foreach ($buscar as $datos) {
											$id_reserva     = $datos['id_reserva'];
											$id_empresa     = $datos['id_empresa'];
											$id_vehiculo    = $datos['id_vehiculo'];
											$id_conductor   = $datos['id_conductor'];
											$id_user        = $datos['id_user'];
											$id_sucursal    = $datos['id_sucursal'];
											$hora_inicio    = $datos['hora_inicio'];
											$hora_fin       = $datos['hora_fin'];
											$fecha_apartado = $datos['fecha_apartado'];
											$observacion    = $datos['observacion'];
											$confirmado     = $datos['confirmado'];
											$fechareg       = date('Y-m-d', strtotime($datos['fechareg']));
											$recepcion      = $datos['recepcion'];

											$recepcion_valor = ($recepcion == 1) ? 'si' : 'no';

											$datos_empresa = $instancia_empresa->mostrarEmpresaIdControl($id_empresa);

											if ($id_sucursal != 0) {

												$datos_sede           = $instancia_sede->mostrarSedeIdControl($id_sucursal);
												$nombre_empresa       = $datos_sede['nombre'];
												$direccion_empresa    = $datos_sede['direccion'];
												$nom_contacto_empresa = $datos_sede['nom_contacto'];
												$telefono_empresa     = $datos_sede['telefono'];
												$estado_empresa       = $datos_sede['estado'];
												$ciudad_empresa       = $datos_sede['ciudad'];
												$nom_contacto         = $datos_sede['nom_contacto'];
											} else {
												$nombre_empresa       = $datos_empresa['nombre'];
												$direccion_empresa    = $datos_empresa['direccion'];
												$nom_contacto_empresa = $datos_empresa['nom_contacto'];
												$telefono_empresa     = $datos_empresa['telefono'];
												$estado_empresa       = $datos_empresa['estado'];
												$ciudad_empresa       = $datos_empresa['ciudad'];
												$nom_contacto         = $datos_empresa['nom_contacto'];
											}

											$datos_vehiculo  = $instancia_vehiculo->mostrarDatosVehiculosIdControl($id_vehiculo);
											$datos_recepcion = $instancia_recepcion->mostrarDatosRecepcionControl($id_reserva, $id_super_empresa);

											$placa = ($recepcion == 0) ? $datos_vehiculo['placa'] : $datos_recepcion['placa_vehiculo'];

											$fecha_actual = date('Y-m-d');

											if ($fecha_apartado <= $fecha_actual && $confirmado == 'si') {
												$alert = '<span class="badge badge-success p-2">Confirmado</span>';
											} else if ($fecha_apartado >= $fecha_actual && $confirmado == 'no') {
												$alert = '<span class="badge badge-warning p-2">Pendiente confirmar</span>';
											} else if ($fecha_apartado > $fecha_actual && $confirmado == 'si') {
												$alert = '<span class="badge badge-danger p-2">No confirmado</span>';
											}

											$datos_consecutivo      = $instancia->mostrarConsecutivoReservaControl($id_reserva);
											$datos_consecutivo_cert = $instancia->mostrarConsecutivoCertReservaControl($id_reserva);

											?>
											<tr class="text-center">
												<td><?=$id_reserva?></td>
												<td><?=$fechareg?></td>
												<td><?=$nombre_empresa?></td>
												<td><?=$placa?></td>
												<td><?=$datos_consecutivo['numero']?></td>
												<td><?=$datos_consecutivo_cert['numero']?></td>
												<td><?=$confirmado?></td>
												<td><?=$ciudad_empresa?></td>
												<td><?=$fecha_apartado?></td>
												<td><?=$nom_contacto?></td>
												<td><?=$recepcion_valor?></td>
												<td><?=$alert?></td>
											</tr>
											<?php
										}
										if (count($buscar) > 0) {
											?>
										</tbody>
										<?php
									}
								}
								?>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
?>
<script src="<?=PUBLIC_PATH?>js/programacion/funcionesProgramacion.js"></script>