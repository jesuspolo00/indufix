<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['nombre_admin'] && $_SESSION['rol'] != 1 && $_SESSION['rol'] != 4) {
    $er = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'PhpSpreadsheet' . DS . 'vendor' . DS . 'autoload.php';
require_once CONTROL_PATH . 'programacion' . DS . 'ControlProgramacion.php';
require_once CONTROL_PATH . 'recepcion' . DS . 'ControlRecepcion.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlEmpresa.php';
require_once CONTROL_PATH . 'empresas' . DS . 'ControlSede.php';
require_once CONTROL_PATH . 'vehiculo' . DS . 'ControlVehiculo.php';
require_once CONTROL_PATH . 'residuos' . DS . 'ControlTipoResiduos.php';

$id_super_empresa = $_SESSION['super_empresa'];

$instancia = ControlProgramacion::singleton_programacion();
$instancia_recepcion = ControlRecepcion::singleton_recepcion();
$instancia_empresa = ControlEmpresa::singleton_empresa();
$instancia_sede = ControlSede::singleton_sede();
$instancia_vehiculo = ControlVehiculo::singleton_vehiculo();
$instancia_tipo_residuo = ControlTipoResiduo::singleton_tipo_residuo();

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

$spreadsheet = new Spreadsheet();

if (isset($_GET['fecha_desde']) && isset($_GET['fecha_hasta'])) {

    $fecha_ini = $_GET['fecha_desde'];
    $fecha_fin = $_GET['fecha_hasta'];
    $buscar = $instancia->buscarReservaFechaControl($fecha_ini, $fecha_fin);
    $buscar_tipo_residuos = $instancia_tipo_residuo->mostrarTipoResiduoControl($id_super_empresa);

    $spreadsheet->getProperties()
        ->setTitle('Reporte')
        ->setDescription('Este documento fue generado por Ecogreen');

    $sheet = $spreadsheet->setActiveSheetIndex(0);


    $estilos_cabecera = [
        'font' => [
            'bold' => true,
        ],
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
    ];

    $estilos_datos = [
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
    ];

    $sheet->getStyle('A1:Z1')->applyFromArray($estilos_cabecera);
    $sheet->getStyle('A2:Z2')->applyFromArray($estilos_cabecera);
    $sheet->getStyle('A:Z')->applyFromArray($estilos_datos);

    //Aumentar y disminuir el rango de el campo de residuo
    $columns_tipo = range('F', 'H');
    $columns_cantidad = range('I', 'K');
    $sheet->mergeCells('F1:H1');
    $sheet->mergeCells('I1:K1');

    $tipo = new MultipleIterator();
    $tipo->attachIterator(new ArrayIterator($columns_tipo));
    $tipo->attachIterator(new ArrayIterator($buscar_tipo_residuos));

    foreach ($tipo as $columnas_tipo) {
        $sheet->setCellValue($columnas_tipo[0] . '2', $columnas_tipo[1]['nombre']);
    }

    $cantidad = new MultipleIterator();
    $cantidad->attachIterator(new ArrayIterator($columns_cantidad));
    $cantidad->attachIterator(new ArrayIterator($buscar_tipo_residuos));

    foreach ($cantidad as $columnas_cantidad) {
        $sheet->setCellValue($columnas_cantidad[0] . '2', $columnas_cantidad[1]['nombre']);
    }

    $columna_inicio = ++$columnas_tipo[0];
    $column_mas = ++$columnas_cantidad[0];

    $sheet->setCellValue('A1', 'FECHA')
        ->setCellValue('B1', 'EMPRESA')
        ->setCellValue('C1', 'PLACA DEL VEHICULO')
        ->setCellValue('D1', 'No. MOVILIZACION')
        ->setCellValue('E1', 'No. RECEPCION')
        ->setCellValue('F1', 'RESIDUOS')
        ->setCellValue($columna_inicio . '1', 'CANTIDADES')
        ->setCellValue($column_mas . '1', 'CERTIFICADO')
        ->setCellValue(++$column_mas . '1', 'CIUDAD')
        ->setCellValue(++$column_mas . '1', 'FECHA DE RECOLECCION')
        ->setCellValue(++$column_mas . '1', 'NOMBRE DEL CONTACTO');


    foreach (range('A', $column_mas) as $column) {
        $sheet->getColumnDimension($column)->setAutoSize(true);
        if (!in_array($column, $columns_tipo) && !in_array($column, $columns_cantidad)) {
            $sheet->mergeCells($column . '1:' . $column . '2');
        } else {
        }
    }

    $cont = 3;

    foreach ($buscar as $datos) {
        $id_reserva = $datos['id_reserva'];
        $id_empresa = $datos['id_empresa'];
        $id_vehiculo = $datos['id_vehiculo'];
        $id_conductor = $datos['id_conductor'];
        $id_user = $datos['id_user'];
        $id_sucursal = $datos['id_sucursal'];
        $hora_inicio = $datos['hora_inicio'];
        $hora_fin = $datos['hora_fin'];
        $fecha_apartado = $datos['fecha_apartado'];
        $observacion = $datos['observacion'];
        $confirmado = $datos['confirmado'];
        $fechareg = date('Y-m-d', strtotime($datos['fechareg']));
        $recepcion = $datos['recepcion'];

        $datos_empresa = $instancia_empresa->mostrarEmpresaIdControl($id_empresa);

        if ($id_sucursal != 0) {
            $datos_sede = $instancia_sede->mostrarSedeIdControl($id_sucursal);
            $nombre_empresa = $datos_sede['nombre'];
            $direccion_empresa = $datos_sede['direccion'];
            $nom_contacto_empresa = $datos_sede['nom_contacto'];
            $telefono_empresa = $datos_sede['telefono'];
            $estado_empresa = $datos_sede['estado'];
            $ciudad_empresa = $datos_sede['ciudad'];
            $nom_contacto = $datos_sede['nom_contacto'];
        } else {
            $nombre_empresa = $datos_empresa['nombre'];
            $direccion_empresa = $datos_empresa['direccion'];
            $nom_contacto_empresa = $datos_empresa['nom_contacto'];
            $telefono_empresa = $datos_empresa['telefono'];
            $estado_empresa = $datos_empresa['estado'];
            $ciudad_empresa = $datos_empresa['ciudad'];
            $nom_contacto = $datos_empresa['nom_contacto'];
        }

        $datos_vehiculo = $instancia_vehiculo->mostrarDatosVehiculosIdControl($id_vehiculo);
        $datos_recepcion = $instancia_recepcion->mostrarDatosRecepcionControl($id_reserva, $id_super_empresa);

        if ($recepcion == 0) {
            $placa = $datos_vehiculo['placa'];
        } else {
            $placa = $datos_recepcion['placa_vehiculo'];
        }


        $datos_consecutivo = $instancia->mostrarConsecutivoReservaControl($id_reserva);
        $datos_consecutivo_cert = $instancia->mostrarConsecutivoCertReservaControl($id_reserva);
        $buscar_tipo_residuo_reserva = $instancia->mostrarTipoResiduoReservaControl($id_reserva);
        $buscar_cantidad_residuo_reserva = $instancia->mostrarCantidadResiduosReservaControl($id_reserva);

        foreach ($buscar_tipo_residuo_reserva as $tipo_reserva) {
            $id_tipo_residuo_reserva = $tipo_reserva['id_tipo_residuo'];

            if ($id_tipo_residuo_reserva == 2) {
                $sheet->setCellValue('F' . $cont, 'X');
            } else if ($id_tipo_residuo_reserva == 3) {
                $sheet->setCellValue('G' . $cont, 'X');
            } else {
                $sheet->setCellValue('H' . $cont, 'X');
            }
        }

        foreach ($buscar_cantidad_residuo_reserva as $cantidad) {
            $id_tipo_residuo_reserva = $cantidad['id_tipo_residuo'];
            $cantidad_total = $cantidad['valor_total'];

            if ($id_tipo_residuo_reserva == 2) {
                $sheet->setCellValue('I' . $cont, $cantidad_total);
            } else if ($id_tipo_residuo_reserva == 3) {
                $sheet->setCellValue('J' . $cont, $cantidad_total);
            } else {
                $sheet->setCellValue('K' . $cont, $cantidad_total);
            }
        }

        $sheet->setCellValue('A' . $cont, $fechareg)
            ->setCellValue('B' . $cont, $nombre_empresa)
            ->setCellValue('C' . $cont, $placa)
            ->setCellValue('D' . $cont, $datos_consecutivo['numero'])
            ->setCellValue('E' . $cont, $datos_consecutivo_cert['numero'])
            ->setCellValue('L' . $cont, $confirmado)
            ->setCellValue('M' . $cont, $ciudad_empresa)
            ->setCellValue('N' . $cont, $fecha_apartado)
            ->setCellValue('O' . $cont, $nom_contacto);
        $cont++;
    }


    $spreadsheet->getActiveSheet()->setTitle('Hoja 1');
    $spreadsheet->setActiveSheetIndex(0);
    $nombreDelDocumento = "Reporte.xlsx";

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . $nombreDelDocumento . '"');
    header('Cache-Control: max-age=0');

    $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    $writer->save('php://output');
}
